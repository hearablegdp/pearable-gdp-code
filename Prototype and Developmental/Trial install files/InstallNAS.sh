#!/bin/bash

#Install requirements for NAS Guide

# Install BlueZ-5  and PulseAudio-5 with Bluetooth support:
apt-get --no-install-recommends install pulseaudio pulseaudio-module-bluetooth bluez

# If your dongle is a based on a BCM203x chipset, install the firmware
apt-get bluez-firmware

# Install MPlayer, along with some codecs, to later test audio output
apt-get install mplayer