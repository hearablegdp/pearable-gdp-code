#!/bin/bash

#Script to update and upgrade new software on RPi
sudo apt-get update
sudo apt-get upgrade
sudo rpi-update
echo "Now Rebooting"
sleep 1
sudo reboot 