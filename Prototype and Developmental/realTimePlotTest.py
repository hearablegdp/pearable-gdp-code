# -*- coding: utf-8 -*-
"""
Spyder Editor

Author: Jordy Williams
This script was used to test real time plotting for buffers in python
run %matplotlib if in spyder
"""

import numpy as np
import matplotlib.pyplot as plt

def plotFromTimeZero():
    """
    This function plots an array of ones in real time from time zero
    this is initiated after an arbetary buffer size
    """
    plt.ion()
    counter = 0  # initilase counter for x axis point (t=0)
    # These would be arrays that are the size of the buffe
    x, y =[], [] # if these were pre allocated arrays set with index might be quicker
    bufferSize = 100
    while True:
        # Appending to x and y each time so plots from t=0
        x.append(np.arange(counter, counter+bufferSize, 1))
        y.append(np.ones(bufferSize))
        plt.plot(x, y, 'b-')
        # How quickly you want the plot to update
        plt.pause(0.05)
        # Skip the array counter by a buffer each time
        counter+=bufferSize
        plt.show()
        
def plotUpdatingBuffer():
    """
    This function plots an array of ones in real time from time zero
    this is initiated after an arbetary buffer size
    """
    plt.ion()
    counter = 0  # initilase counter for x axis point (t=0)
    bufferCounter = 0 # counting number of buffers
    bufferSize = 100 # in sameples
    N = 5 # number of buffers to plot
    while True:
        x_buffer = np.arange(counter, counter+bufferSize, 1)
        y_buffer = np.ones(bufferSize)
        # Appending to x and y the counter is less than number of buffers
        if bufferCounter % N == 0:  # if bufferCounter is multiple of N
            plt.close()  # restart plot
        plt.plot(x_buffer, y_buffer, 'b-')
        # How quickly you want the plot to update
        plt.pause(0.5)
        # Skip the array counter by a buffer each time
        counter+=bufferSize # shift counter for x array by a buffer
        bufferCounter+=1  # 
        plt.show()