import numpy as np
import pylab as py

def calculate(x,N,env,T,CR,KW,MG,y):
    i = 0
    # setting up array for ydB and xdB
    ydB=np.zeros_like(y)
    xdB = np.zeros_like(x)
    while i < N-1:
#        zeroIdx = np.where(x[:,i]==0) # zero idx for dB cannot get inf value
        nonZeroIdx = np.where(x[:,i]!=0)
        # want to only calc dB of nonzero values
        xdB[nonZeroIdx, i] = 20*np.log10(np.abs(x[nonZeroIdx, i]))
        envdB = 20*np.log10(np.abs(env[:,i]))
        # Using np.where to set each channel, instead of if statements
        # constant gain region of graph
        cstGainIdx = np.where(2*(envdB-T)< -KW)
        # Compression region of graph
        compressionIdx = np.where((2*np.abs(envdB-T))<=KW)        
        # limiting region of graph
        limitingIdx = np.where((2*(envdB-T)) > KW)
        # Setting ydB indicies to be the approprite gain      
#        ydB[:,cstGainIdx] = xdB[cstGainIdx]
        ydB[cstGainIdx, i] = xdB[cstGainIdx, i]        
#        ydB[:,compressionIdx] = xdB[compressionIdx]+(1/CR-1)*((
#                xdB[compressionIdx]-T+(KW/2))**2)/(2*KW)
        ydB[compressionIdx, i] = xdB[compressionIdx, i]+(1/CR-1)*((
                xdB[compressionIdx, i]-T+(KW/2))**2)/(2*KW)
#        ydB[:,limitingIdx] = T+((xdB[limitingIdx]-T)/CR)
        ydB[limitingIdx, i] = T+((xdB[limitingIdx, i]-T)/CR)
        # Adding markup gain
        ydB[:, i] = ydB[:, i] + MG
        # converting to linear
#        y = 10**(ydB/20)
#        y[:, i] = 10**(ydB[:, i]/20)
##        y=0
#        y[zeroIdx, i]=0
#        # getting negative idicies to convert back to negative
#        negIdx = np.where(x<0)
##        negIdx = np.where(x[:,i]<0)
#        # Setting to be negative
#        y[negIdx] = -y[negIdx]
##        y[negIdx, i] = -y[negIdx, i]
#        i+=1
        
        i+=1   
    #### Alternative use of y outside while loop
    y = 10**(ydB/20)
#        y=0
        
    zeroIdx = np.where(x==0)
    y[zeroIdx]=0
        # getting negative idicies to convert back to negative
    negIdx = np.where(x<0)
#        negIdx = np.where(x[:,i]<0)
        # Setting to be negative
    y[negIdx] = -y[negIdx]
#        y[negIdx, i] = -y[negIdx, i]
    return(y)
