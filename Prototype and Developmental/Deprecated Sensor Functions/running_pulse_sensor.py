from heart_rate_v2 import Pulsesensor
import time

p = Pulsesensor()
p.startAsyncBPM(P=512, T=512, thresh=525, amp=100)
# 512 as half of 1024 due to wave form 2**10
try:
    while True:
        bpm = p.BPM
        if bpm > 0:
            print("BPM: %d" % bpm)
        else:
            print(str(bpm))
            print("No Heartbeat found")
        time.sleep(1)
except:
    p.stopAsyncBPM()
