import time
import Adafruit_MCP3008

# Software SPI configuration:
CLK  = 18
MISO = 23
MOSI = 24
CS   = 25
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)


def read_channel(channel):
    """Reads the data from ADC channels"""
    data = mcp.read_adc(channel)
    return data


def analog_voltage(adc):
    return adc.value/1024 *adc.reference_voltage
volts = analog_voltage(read_channel(3))
print('Photocell Voltage: {0}V'.format(volts))
