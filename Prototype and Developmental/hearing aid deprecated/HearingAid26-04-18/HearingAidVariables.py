#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 11:51:01 2018

@author: pi
"""
import numpy as np

"""The following variables are set for all octave bands for left and right
channels the first five correspond to the left channel and next five, the right
channel"""
# octave band center frequencies
bands_def = np.array([250, 500, 1000, 2000, 4000])
# Gain
G_def = np.array([1., 1., 1., 1., 1.,1., 1., 1., 1., 1.])
# Threshold
T_def = np.array([0., 0., 0., 0., 0.,0., 0., 0., 0., 0.])
# Compression ratio
CR_def = np.array([1., 1., 1., 1., 1.,1., 1., 1., 1., 1.])
# Knee width
KW_def = np.array([10., 10., 10., 10., 10.,10., 10., 10., 10., 10.])
# Makeup gain
MG_def = np.array([0., 0., 0., 0., 0.,0., 0., 0., 0., 0.])

def numBands(indxBands, bands_def=bands_def, G_def=G_def, T_def=T_def,
             CR_def=CR_def, KW_def=KW_def, MG_def=MG_def):
    bands = bands_def[indxBands]
    indxLR = np.concatenate((indxBands, indxBands+len(bands_def)))
    T = T_def[indxLR]
    CR = CR_def[indxLR]
    KW = KW_def[indxLR]
    MG = MG_def[indxLR]
    G = G_def[indxLR]
    return(bands, G, T, CR, KW, MG)

