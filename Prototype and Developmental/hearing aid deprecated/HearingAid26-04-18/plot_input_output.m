x = load('input_FFT.txt');
y = load('output_FFT.txt');

figure(1)
plot(x)
hold on
plot(y)
legend('input','output')

x = load('input_FIR.txt');
y = load('output_FIR.txt');

figure(2)
plot(x)
hold on
plot(y)
legend('input','output')

x = load('input.txt');
y = load('output.txt');

figure(3)
subplot(2,1,1)
plot(x)
subplot(2,1,2)
plot(y)
legend('input','output')

%% Sound meter level recordings

dB1 = 90.4
dB2 = 85.2 % Half amplitude
dB3 = 79.1 % Half again

x1 = load('input_79.1.txt');
x2 = load('input_85.2.txt');
x3 = load('input_90.4.txt');

max_x = [max(x1) max(x2) max(x3)]

sound(x1,12000)

%%

x = load('input.txt');
y = load('output.txt');

figure(3)
subplot(2,1,1)
plot(x)
subplot(2,1,2)
plot(y)
legend('input','output')


%%

sound(x,12000)