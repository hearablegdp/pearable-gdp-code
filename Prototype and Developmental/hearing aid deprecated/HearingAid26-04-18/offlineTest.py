#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 15:16:29 2018

@author: pi
This script was written in order to plot the offline results of the functions
to be run in real-time using VISR
"""
import pylab as py
import envelope_function, compression_function, filtFreq_function, gain_python, filtFreq_function_fast
import numpy as np
from scipy.io import wavfile
import time
import HearingAidVariables as HA

# Offline tests of compression and envelope algorithms


def offlineHA(bandIndex, wavName, oneBuffer=True, impulse=False, seconds=None,
              blockLength=512, order=10, width=2):
    """
    Performs the hearing aid processing on the wav file provided.
    If OneBufer is True then processing is done on one buffer only and results
    for the speed of the code is printed, telling you if it is fast enough
    If oneBuffer is false then the length of data is given by the length of the
    wavfile
    If a value for seconds is given, the computation will be done that many
    seconds on the wavfile
    Parameters
    ----------
    bandIndex: ndarry
        index of the bands being used out of [250, 500, 1k, 2k, 4k] Hz
    wavname: string
        name of the wavfile to read
    oneBuffer: Boolean
        determines whether to perform the test over one buffer only
    impulse
    """
    bands, G, T, CR, KW, MG = HA.numBands(bandIndex)
    print('Bands used: '+str(bands))
    fs, data = wavfile.read(wavName+'.wav')
    data = data.reshape([data.shape[1], data.shape[0]])  # swapping order of data
    if oneBuffer:
        if impulse:
            data = np.zeros_like(data[:, 0:blockLength])
            data[0]=1
        else:
            data = data[:, 0:blockLength]
    elif seconds is None:
        pass
    else:
        data = data[:, 0:seconds*fs] 
    t = np.arange(0, data.shape[1])/float(fs)
    allowedTime = float(blockLength)/fs
    print('Allowed Time: '+str(allowedTime))  
    tFiltStart = time.time()
    print(type(bands))
    b, a = filtFreq_function_fast.getFilterCoeffs(fs, bands, order)
    filt = filtFreq_function_fast.calculate(data, b, a, width, bufferSize=blockLength, bands=bands)
    filtTime = time.time()-tFiltStart
    print("Filter code needs %f seconds" % (filtTime))
    tEvStart = time.time()
    ev = envelope_function.calculate(filt)
    evTime = time.time()-tEvStart
    print("Envelope code needs %f seconds" % (evTime))
    tComStart = time.time()
    com=compression_function.calculate(filt,ev,T,CR,KW,MG)
    comTime = time.time()-tComStart
    print("Compression code needs %f seconds" % (comTime))
    tGainStart = time.time()
    gain=gain_python.calculate(com,G)
    gainTime = time.time()-tGainStart
    print("Gain code needs %f seconds" % (gainTime))
    totalTime = time.time()-tEvStart
    output=gain
    if oneBuffer:
        if allowedTime>totalTime:
            print("Code is fast enough. Time remaining: %f seconds." % (allowedTime-totalTime))
        else:
            print("Code takes %f seconds too long" % (totalTime-allowedTime))
    else:
        outWav = com.reshape([output.shape[1], output.shape[0]])  # swapping order of data
        writeName = wavName+str('HearingAided.wav')
        wavfile.write(writeName, fs, outWav/np.max(outWav))
    return(filt, ev, com, output, t, data, fs)


testFilename = 'snarky' # this is 2ch 48000 fs file
bandIndex = np.array([0, 1, 2, 3, 4])
filt, ev, com, output, t, data, fs = offlineHA(bandIndex, wavName=testFilename,oneBuffer=True, order=2)

# %% plotting filter
#FILT1 = np.fft.fft(filt)
#DATA = np.fft.fft(data)
#f = np.linspace(0, fs, len(t))
#py.close('all')
#py.figure()
#py.semilogx(f[:len(t)/2], np.transpose(DATA[:, :len(t)/2]))
##py.semilogx(f[:len(t)/2], np.transpose(FILT1[:,:len(t)/2]))
#py.ylim(-2, 2)
#py.xlim(0, 8000)
# %% Plotting - envelope
#py.figure()
#py.plot(t, data[0],color='C0', label='wav file - L')
##py.plot(t, data[1],color='C0', ls='--', label='wav file - R')
#py.plot(t, ev[0],color='C1', label='Envelope - L')
##py.plot(t, ev[1],color='C1',ls='--', label='Envelope - R')
#py.legend(loc=0)
#py.xlabel(r'$Time - [s]$')
#py.ylabel(r'$Amplitude - [a.u.]$')
#py.title('Offline processing of Signal Evelope of {} File'.format(testFilename))
#py.savefig('EnvelopePlot')
#py.show()

## %% Plotting - compression
#py.figure()
#py.plot(t, data[0],color='C0', ls='-',label='wav file - L')
#py.plot(t, data[1],color='C0', ls='--',label='wav file - R')
#py.plot(t, com[0],color='C1', ls='-',label='Compressed - L')
#py.plot(t, com[1],color='C1', ls='--',label='Compressed - R')
#py.legend(loc=0)
#py.xlabel(r'$Time - [s]$')
#py.ylabel(r'$Amplitude - [a.u.]$')
#py.title('Offline processing of Compression of {} File'.format(testFilename))
#py.savefig('CompressionPlot')
#py.show()