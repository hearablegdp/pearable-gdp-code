#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 01:56:22 2018

@author: pi
"""

import VISRComponents
import visr
import rrl
import audiointerfaces as ai
import time
import numpy as np

width = 2
fs = 44100  # sampling frequency (JACK interface fs set to match this)
blockSize = 512  # block/buffer size 512
G = -6  # dB gain for the gain function gain_python
tauA, tauR = 0.005, 0.02
env = np.zeros([width, blockSize])
T, CR, KW, MG = 0., 1., 10., 0.

c = visr.SignalFlowContext(blockSize, fs)  # sets the sampling frequency and
# buffer size in VISR
#pa = VISRComponents.PythonAdder(c, "pa0", None, 1, 2)  
#pa1 = VISRComponents.Pearable2(c, "pa0", None, width, G, tauA, tauR, env, T, CR,
#                              KW, MG) 
pa1 = VISRComponents.Pearable(c, "pa0", None, width, G, tauA, tauR, env, T, CR,
                              KW, MG) 
flow = rrl.AudioSignalFlow(pa1)
# the component containing the processing functionality
aiConfig = ai.AudioInterface.Configuration(flow.numberOfCaptureChannels,
                                           flow.numberOfPlaybackChannels,
                                           fs,
                                           blockSize)
#aiConfig1 = ai.AudioInterface.Configuration(flow1.numberOfCaptureChannels,
                                           #flow1.numberOfPlaybackChannels,
                                           #fs,
                                           #blockSize)
# defines an audiointerface config file to be used in audio interface factory
# containing number of channels

#jackCfg = """{ "clientname": "Pearable",
#  "autoconnect" : "true",
#  "portconfig":
#  {
#    "capture":  [{ "basename":"in_", "externalport" : {} }],
#    "playback": [{ "basename":"out_", "externalport" : {} }]
#  }
#}
#"""
jackCfg = """{ "clientname": "Pearable1",
  "autoconnect" : "true",
  "portconfig":
  {
    "capture":  [{ "basename":"in_", "externalport" : {} }],
    "playback": [{ "basename":"out_", "externalport" : {} }]
  }
}
"""
#jackCfg1 = """{ "clientname": "gain_function",
#  "autoconnect" : "true",
#  "portconfig":
#  {
#    "capture":  [{ "basename":"in_", "externalport" : {} }],
#    "playback": [{ "basename":"out_", "externalport" : {} }]
#  }
#}

# jack config file

aIfc = ai.AudioInterfaceFactory.create("Jack", aiConfig, jackCfg)
#aIfc1 = ai.AudioInterfaceFactory.create("Jack", aiConfig1, jackCfg1)

aIfc.registerCallback(flow)
#aIfc1.registerCallback(flow1)

aIfc.start()
#aIfc1.start()

print("Rendering started.")

time.sleep(5)
i = input("Enter text (or Enter to quit): ")
if not i:
    aIfc.stop()
    aIfc.unregisterCallback()
    del aIfc