#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 14:53:12 2018

@author: pi
"""

import numpy as np

x = np.array([[10, 12], [3, 5], [16, 32]])
env = np.array([[20, 8], [3, 6], [22, 14]])
tauA=0.5
tauR = 0.4
fs = 4100
N = len(env[0, :])

alphaA = np.exp(-1/(tauA*fs))
alphaR = np.exp(-1/(tauR*fs))



x_abs = np.array([10, 20])

i=0
alpha = np.zeros(2)
x_abs = np.abs(x[:][i])

y0 = env[:][i-1]
aIdx = np.where(x_abs > y0)
rIdx = np.where(x_abs <= y0)

alpha[aIdx] = alphaA
alpha[rIdx] = alphaR

env[:][i] = alpha * y0 + (1 - alpha) * x_abs;

env0 = env[:][N-1];
