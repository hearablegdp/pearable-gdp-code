#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import numpy as np


def compress(x, env, T, CR, KW, MG, feedThrough=False):
    """
    Compresses a signal given the envelope and other compression parameters
    Parameters
    ----------
    x: ndarray
        The signal that will be compressed
    env: ndarray
        The envelope of x (must have same size as x)
    Hearing aid parameters:
    The following parameters are arrays of floats to be applied to each octave
    band in two channels (length up to 10 for 2 channels and 5 octave bands)
    T: ndarray
        compression thresholds
    CR: ndarry
        compression ratios
    KW: ndarry
        knee widths
    MG: ndarray
        make-up gains
    feedThrough: Boolean
        if True feed through audio only

    Returns
    -------
    y: ndarray
        the compressed signal
    """
    # if feedThrough is True return the input as the output
    if feedThrough:
        return(x)
    # create an empty array to assign the decibel valued input x
    xdB = np.zeros_like(x)
    # create an empty array to assign the decibel valued output y
    ydB = np.zeros_like(x)
    # create an empty array to assign the decibel valued envelope env
    envdB = np.zeros_like(env)
    # transposes the hearing aid variables for the mathematics to work across
    # the correct dimensions.
    T_T, KW_T, MG_T = T[:, None], KW[:, None], MG[:, None]
    # find where x is not zero
    nonZeroIdx = np.where(x != 0)
    # find where env is not zero
    nonZeroEnvIdx = np.where(env != 0)
    # use non-zero index to convert x to dB (this avoids division of zero)
    xdB[nonZeroIdx] = 20*np.log10(np.abs(x[nonZeroIdx]))
    # use non-zero index to convert env to dB (this avoids division of zero)
    envdB[nonZeroEnvIdx] = 20*np.log10(np.abs(env[nonZeroEnvIdx]))

    # COMPRESSION STUFF RESEARCH THIS TO EXPLAIN WHATS GOING ON
    cstGainIdx = np.where(2*(envdB-T_T) < -KW_T)
    compressionIdx = np.where((2*np.abs(envdB-T_T)) <= KW_T)
    limitingIdx = np.where((2*(envdB-T_T)) > KW_T)

    ydB[cstGainIdx] = xdB[cstGainIdx]
    ydB[compressionIdx] = xdB[compressionIdx]+(1/CR[compressionIdx[0]]-1)*((
            xdB[compressionIdx]-T[compressionIdx[0]]+(
                    KW[compressionIdx[0]]/2))**2)/(2*KW[compressionIdx[0]])
    ydB[limitingIdx] = T[limitingIdx[0]]+((xdB[limitingIdx]-T[
            limitingIdx[0]])/CR[limitingIdx[0]])
    ydB = ydB + MG_T
    # anti-log ydB
    y = 10**(ydB/20)
    # find where x is zero
    zeroIdx = np.where(x == 0)
    # assign these zeros to y
    y[zeroIdx] = 0
    # find where x is negative
    negIdx = np.where(x < 0)
    # make y negative in these locations
    y[negIdx] = -y[negIdx]
    return(y)
