#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Tue Mar 20 01:56:22 2018

@author: pi
"""
# VISR modules:
import visr
import rrl
import audiointerfaces as ai

# scripts written for Pearable GDP:
import VISRComponents
import hearingAidVariables as HA

# other modules:
import time
import numpy as np
import pylab as plt
from scipy.io import wavfile
"""
========
Overview:
========
This script is the shell that runs the VISR components found in
VISRComponents.py. It is split into three sections:
1. Set up the variables
2. Setup the signal flow
3. Run the signal flow
"""


"""
========================
Part 1: Set up variables
========================
"""

fs, data = wavfile.read('snarky'+'.wav')
data = data.T
inputSignal = data

'''SIGNAL PROCESSING VARIABLES'''
# number of audio channels (2 for stereo)
width = 2
# sampling frequency, if using JACK server this must be the same as the Jack
# server is using. This may also be limitted by the sound card's minimum
# sampling frequency
#fs = 32000
# block or buffer size, this determines how many samples is sampled at a time
# the 'allowed time'  will be governed by blockSize/fs. This is also the
# software latency
blockSize = 4096
# the order chosen for the band pass filters that split the signal into
# seperate octave bands
bandOrder = 2
# for feedback cancelation the signal will be shifted in the frequency domain
# fShift is the value in Hz to shift the signal up by
fShift = 1
# variable that indexes which bands to use out of the following:
# 250 Hz, 500 Hz, 1000 Hz, 2000 Hz, 4000 Hz.
# This is given to HA.numBands() to get the hearing aid variables


'''HEARING AID PARAMETERS'''
bandIndex = np.array([2])
# used HA.numbands() to get the hearing aid variables in the right format
# returning centre frequencies of chosen bands bands, gain G, compression
# threshold T, compression ratio CR, knee width KW and makeup gain MG.
bands, G, T, CR, KW, MG, tauA, tauR = HA.numBands(bandIndex)


'''BOOLEANS'''
# if feedThrough is true everything will be ignored and a simple atomic
# component feedthrough will be used instead, this can be useful to see if
# your audio system is set up properly
feedThrough = False
# the following Booleans are used to choose whether to ignore the signal
# processing in the functions. If they are True, the function will just
# feedthrough the audio instead of performing the DSP

# Boolean feed through choice for frequency shift
fTFeedbackCont = True
# Boolean feed through choice for octave band filtering
fTBands = False
# Boolean feed through choice for envelope
fTEnvelope = False
# Boolean feed through choice for compression
fTCompression = False
# Boolean feed through choice for noise reduction
fTNoiseRed = False
# Boolean feed through choice for gain
fTGain = False

# Boolean that decides which feedback control to use, if True frequency
# shifting is used if False, notch filtering is used
notchFeedback = False
# Boolean that decides which envelope function to use, if True the hilbert
# transform is used, if False, a custom envelope function is used that allows
# customisable parameters
hilbertEnv = False
# choosed what server to use, if False PortAudio is used, if True JackAudio is
# used
Jack = False


"""
==============================
Part 2: Set up the signal flow
==============================
"""
# set up the sampling frequency and buffer size in VISR
c = visr.SignalFlowContext(blockSize, fs)
if feedThrough:
    pa1 = VISRComponents.feedThrough(context=c, name="FT", parent=None,
                                     width=width)
# runs the composite component Pearable from VISRComponents.py which contains
# the audio connections between the different signal processing classes
# (atomic components).
else:
    pa1 = VISRComponents.Pearable(context=c, name="pa0", parent=None,
                                  width=width, bands=bands,
                                  bandOrder=bandOrder,
                                  G=G, T=T, CR=CR, KW=KW, MG=MG,
                                  tauA=tauA, tauR=tauR,
                                  fShift=fShift,
                                  notchFeedback=notchFeedback,
                                  hilbertEnv=hilbertEnv,
                                  fTFeedbackCont=fTFeedbackCont,
                                  fTBands=fTBands,
                                  fTEnvelope=fTEnvelope,
                                  fTCompression=fTCompression,
                                  fTNoiseRed=fTNoiseRed, fTGain=fTGain)
# Or a more configurable Python adder
# pa = PythonAdder( c, "pa0", None, 3, 2 )

numBlocks = 16
numSamples = numBlocks*blockSize

t = np.arange(0, numSamples, dtype=np.float32)/fs


#inputSignal = np.zeros([width, numSamples], dtype=np.float32)
#inputSignal[0, :] = np.sin(2*np.pi*440 * t)
#inputSignal[1, :] = 0.5*np.sin(2*np.pi*880 * t)
#inputSignal[2, :] = 0.15*np.sin(2*np.pi*1340 * t)
#
#referenceOutput = inputSignal[0:2, :] + inputSignal[2:4, :] +\
# inputSignal[4:6, :]

outputSignal = np.zeros((width, numSamples), dtype=np.float32)


flow = rrl.AudioaalFlow(pa1)

#outputSignal = flow.process(inputSignal)
for blockIdx in range(0, numBlocks):
    inputBlock = inputSignal[:, blockIdx*blockSize:(blockIdx+1)*blockSize]
    outputBlock = flow.process(inputBlock)
    outputSignal[:, blockIdx*blockSize:(blockIdx+1)*blockSize] = outputBlock

plt.figure(1)
plt.plot(t, referenceOutput[0, :], 'bo-', t, outputSignal[0, :], 'rx-')
plt.legend(['reference', 'output'])
plt.xlabel('Time s')
plt.show()
