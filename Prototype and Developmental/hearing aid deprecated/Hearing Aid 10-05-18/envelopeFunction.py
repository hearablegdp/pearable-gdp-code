#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import numpy as np
from scipy.signal import hilbert


def envelope(x, feedThrough=False):
    """
    Find the envelope of the given signal using the hilbert trnsform
    Parameters
    ----------
    x: ndarry
        the signal to be enveloped
    Returns
    -------
    envelope: ndarry
        the enveloped signal
    """
    # if feedThrough is True return the input as the output
    if feedThrough:
        return(x)
    # uses the scipy.signal module hilbert to get the hilbert transform
    analytic_signal = hilbert(x)
    # absolutes the hilbert transform to be only positive
    envelope = np.abs(analytic_signal)
    return(envelope)
