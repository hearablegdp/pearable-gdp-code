#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Wed Mar 21 12:27:44 2018

@author: pi
"""

# import relavent modules:
import visr
import rbbl
import rcl
import numpy as np

# import scripts written for Pearable GDP:
import freqShiftFunction
import feedbackControlNotch
import octaveBandFilt
import envelopeFunction
#import envelopeControl
import envelopeFaster as envelopeControl
import noiseReduction
import compressionFunction
import compressionFunctionInterp
import gainFunction
import octaveSum


"""
===============================================================================
            COMPOSITE COMPONENT CONTROLS THE SIGNAL FLOW
===============================================================================
"""


class Pearable(visr.CompositeComponent):
    """
    Composite component that sets up the following signal flow:
    input --> octave band filtering --> envelope --> noise reduction -->
    compression --> gain --> octave band summing --> output

    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    width: integar
        number of channels
    bands: ndarray
        array containing the centre frequencies of the octave bands being used
    bandOrder: integar
        order to be given to the band pass filters that will split the signal
        into frequency bands

    Hearing aid parameters:
    The following parameters are arrays of floats to be applied to each octave
    band in two channels (length up to 10 for 2 channels and 5 octave bands)
    G: ndarry
        gains
    T: ndarray
        compression thresholds
    CR: ndarry
        compression ratios
    KW: ndarry
        knee widths
    MG: ndarray
        make-up gains
    """
    def __init__(self, context, name, parent, width, bands, bandOrder, G, T,
                 CR, KW, MG, tauA, tauR, fShift, notchFeedback, hilbertEnv,
                 fTFeedbackCont, fTBands, fTEnvelope, fTCompression,
                 fTNoiseRed, fTGain):
        super(Pearable, self).__init__(context, name, parent)
        # number of channels after the input signal has been filtered into
        # multiple octave bands
        processWidth = width*len(bands)
        """
        ===============================
        Set up the external audio ports
        ===============================
        """
        # define the input
        self.input = visr.AudioInputFloat(name="in", parent=self, width=width)
        if notchFeedback:
            self.feedbackControl = notchFilt(context=context, name="notchFilt",
                                             parent=self, width=width,
                                             fTFeedbackCont=fTFeedbackCont)
        else:
            # call the class freqShift() for the frequency domain shifting of
            # the signal, this is for feedback control
            self.feedbackControl = freqShift(context=context, name="freqShift",
                                             parent=self, width=width,
                                             fShift=fShift,
                                             fTFeedbackCont=fTFeedbackCont)
        # call the class filtFreqFast() for the octave band filtering part of
        # the flow
        self.octBands = filtFreqFast(context=context, name="bands",
                                     parent=self, width=width,
                                     processWidth=processWidth, bands=bands,
                                     bandOrder=bandOrder, fTBands=fTBands)
#        self.octBands = biquadVISR(context=context, name="bands",
#                                   parent=self, width=width,
#                                   processWidth=processWidth, bands=bands,
#                                   bandOrder=bandOrder)

        # call the class envelopeCont() for the envelope part of the flow which
        # allows the change of attack and release time
        # the envelope

        # if envType is chosen to be hilbert, use the hilbert transform to find
        # call the class compression() for the compression part of the flow
        if hilbertEnv:
            self.env = envelope(context=context, name="e", parent=self,
                                processWidth=processWidth, bands=bands,
                                fTEnvelope=fTEnvelope)
        else:
            self.env = envelopeCont(context=context, name="e", parent=self,
                                    processWidth=processWidth, bands=bands,
                                    tauA=tauA, tauR=tauR,
                                    fTEnvelope=fTEnvelope)
        self.nR = noiseRed(context=context, name="NR", parent=self,
                           processWidth=processWidth,
                           fTNoiseRed=fTNoiseRed)
        self.comp = compression(context=context, name="c", parent=self,
                                processWidth=processWidth,
                                T=T, CR=CR, KW=KW, MG=MG,
                                fTCompression=fTCompression)
        # call the class gain() for the gain part of the flow
        self.gain = gain(context=context, name="g", parent=self,
                         processWidth=processWidth, G=G, fTGain=fTGain)
        # call the class octSum() for the octave band summing part of the flow
        self.sum = octSum(context=context, name="OctaveSum", parent=self,
                          width=width, processWidth=processWidth,
                          fTOctSum=fTBands)
        # define the ouput
        self.output = visr.AudioOutputFloat(name="out", parent=self,
                                            width=width)
        """
        ============================
        Set up the audio connections
        ============================
        """
        # input --> frequency shifting
        self.audioConnection(self.input,  self.feedbackControl.audioPort("in"))
        # frequency shifting --> octave band filtering
        self.audioConnection(self.feedbackControl.audioPort("out"),
                             self.octBands.audioPort("in"))
        # octave band filtering --> envelope
        self.audioConnection(self.octBands.audioPort("out"),
                             self.env.audioPort("in"))
        # octave band filtering --> noise reduction
        self.audioConnection(self.octBands.audioPort("out"),
                             self.nR.audioPort("in"))
        # envelope --> noise reduction
        self.audioConnection(self.env.audioPort("out"),
                             self.nR.audioPort("env"))
        # noise reduction --> compression
        self.audioConnection(self.nR.audioPort("out"),
                             self.comp.audioPort("in"))
        # envelope --> compression
        self.audioConnection(self.env.audioPort("out"),
                             self.comp.audioPort("env"))
        # compression --> gain
        self.audioConnection(self.comp.audioPort("out"),
                             self.gain.audioPort("in"))
        # gain --> octave band summing
        self.audioConnection(self.gain.audioPort("out"),
                             self.sum.audioPort("in"))
        # octave band filtering --> output
        self.audioConnection(self.sum.audioPort("out"), self.output)


"""
==============================================================================
            ATOMIC COMPONENTS CONTROL THE DSP PROCESSES
==============================================================================
"""


class feedThrough(visr.AtomicComponent):
    """
    FEEDTHROUGH AUDIO
    -----------------------------------------
    Atomic component that performs feed through audio only

    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    width: integar
        number of channels

    Output
    ------
    y: 2D array of shape (buffer size, processWidth)
        input signal devided into octave bands
    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, width):
        super(feedThrough, self).__init__(context, name, parent)
        # set up the input
        self.input = visr.AudioInputFloat("in", self, width)
        # set up the output
        self.output = visr.AudioOutputFloat("out", self, width)
    """
    ==================
    Define the process
    ==================
    """
    def process(self):
        # feedthrough audio
        self.output.set(self.input.data())


class freqShift(visr.AtomicComponent):
    """
    FEEDBACK CONTROL USING FREQUENCY SHIFTING
    -----------------------------------------
    Atomic component that performs frequency shifting of the signal for
    feedback control
    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    width: integar
        number of channels
    fShift: integar
        the desired frequency to shift the signal by
    fTFeedbackCont: Boolean
        if True the function will feed through audio only

    Output
    ------
    y: 2D array of shape (buffer size, processWidth)
        input signal devided into octave bands
    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, width, fShift, fTFeedbackCont):
        super(freqShift, self).__init__(context, name, parent)
        # set up the input
        self.input = visr.AudioInputFloat("in", self, width)
        # set up the output
        self.output = visr.AudioOutputFloat("out", self, width)
        # redefine fShift as a variable that can be used in process
        self.fShift = fShift
        # redefine fTFeedbackCont as a variable that can be used in process
        self.fTFeedbackCont = fTFeedbackCont
    """
    ==================
    Define the process
    ==================
    """
    def process(self):
        # input data
        x = self.input.data()
        # call a function from the script freqShiftFunction.py that takes
        # the signal x and shifts it by frequency fShift
        y = freqShiftFunction.shift(x, self.period(), self.samplingFrequency(),
                                    fShift=self.fShift,
                                    feedThrough=self.fTFeedbackCont)
        # set y as the signal flow output
        self.output.set(y)


class notchFilt(visr.AtomicComponent):
    """
    FEEDBACK CONTROL USING NOTCH FILTERING
    --------------------------------------
    Atomic component that uses peak detection and notch filtering to to prevent
    feedback
    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    width: integar
        number of channels
    fShift: integar
        the desired frequency to shift the signal by
    fTFeedbackCont: Boolean
        if True the function will feed through audio only

    Output
    ------
    y: 2D array of shape (buffer size, processWidth)
        input signal devided into octave bands
    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, width, fTFeedbackCont):
        super(notchFilt, self).__init__(context, name, parent)
        # set up the input
        self.input = visr.AudioInputFloat("in", self, width)
        # set up the output
        self.output = visr.AudioOutputFloat("out", self, width)
        # redefine width as a variable that can be used in process
        self.width = width
        # redefine fTFeedbackCont as a variable that can be used in process
        self.fTFeedbackCont = fTFeedbackCont
    """
    ==================
    Define the process
    ==================
    """
    def process(self):
        # input data
        x = self.input.data()
        # call a function from the script feedbackControlNotch.py that takes
        # the signal x, if a peak in the spectrum is deemed too high in
        # magnitude it will notch filter it
        y = feedbackControlNotch.notch(x, self.width, self.period(),
                                       self.samplingFrequency(),
                                       feedThrough=self.fTFeedbackCont)
        # set y as the signal flow output
        self.output.set(y)


class filtFreqFast(visr.AtomicComponent):
    """
    Atomic component that performs the octave band filtering
    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    width: integar
        number of channels
    processWidth: integar
        number of channels to convert to (width*number of bands)
    bands: ndarray
        array containing the centre frequencies of the octave bands being used
    bandOrder: integar
        order to be given to the band pass filters that will split the signal
        into frequency bands

    Output
    ------
    y: 2D array of shape (buffer size, processWidth)
        input signal devided into octave bands
    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, width, processWidth,
                 bands, bandOrder, fTBands):
        super(filtFreqFast, self).__init__(context, name, parent)
        # set up the input
        self.input = visr.AudioInputFloat("in", self, width)
        # set up the output
        if fTBands:
            self.output = visr.AudioOutputFloat("out", self, width)
        else:
            self.output = visr.AudioOutputFloat("out", self, processWidth)
        # define bands and width as variables that can be referenced in process
        self.bands = bands
        self.width = width
        # call a function from the script filtFreqFunctionFast.py to get the
        # filter coefficients that will split the signal into octave bands
        self.b, self.a = octaveBandFilt.getFilterCoeffs(
                self.samplingFrequency(), bands, bandOrder)
        self.fTBands = fTBands
    """
    ==================
    Define the process
    ==================
    """
    def process(self):
        # input data
        x = self.input.data()
        # call a function from the script filtFreqFunctionFast.py that takes
        # the a and b coefficients found previously to split x into octave
        # bands
        y = octaveBandFilt.applyFilterCoeffs(x, self.b, self.a, self.width,
                                             self.period(), self.bands,
                                             feedThrough=self.fTBands)
        # set y as the signal flow output
        self.output.set(y)


class biquadVISR(visr.AtomicComponent):
    """
    Atomic component that performs the octave band filtering
    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    width: integar
        number of channels
    processWidth: integar
        number of channels to convert to (width*number of bands)
    bands: ndarray
        array containing the centre frequencies of the octave bands being used
    bandOrder: integar
        order to be given to the band pass filters that will split the signal
        into frequency bands

    Output
    ------
    y: 2D array of shape (buffer size, processWidth)
        input signal devided into octave bands
    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, width, processWidth,
                 bands, bandOrder):
        super(biquadVISR, self).__init__(context, name, parent)
        # set up the input
        self.input = visr.AudioInputFloat("in", self, width)
        # set up the output
        self.output = visr.AudioOutputFloat("out", self, processWidth)
        # define bands and width as variables that can be referenced in process
        self.bands = bands
        self.width = width
        self.processWidth = processWidth
        self.context = context  # this might not be needed??
        # call a function from the script filtFreqFunctionFast.py to get the
        # filter coefficients that will split the signal into octave bands
        self.b, self.a = octaveBandFilt.getFilterCoeffs(
                self.samplingFrequency(), bands, bandOrder)
    """
    ==================
    Define the process
    ==================
    """
    def process(self):
        # input data
        x = self.input.data()
        # call a function from the script filtFreqFunctionFast.py that takes
        # the a and b coefficients found previously to split x into octave
        # bands
        y = octaveBandFilt.applyFilterCoeffs(x, self.b, self.a, self.width,
                                             self.period(), self.bands)
        # set y as the signal flow output
        self.output.set(y)
        lfFilter = rbbl.BiquadCoefficientFloat(1.0, -3.7309821933422431,
                                               5.2918958422735862,
                                               -3.3812970922825225,
                                               0.82172646376048009)
        hfFilter = rbbl.BiquadCoefficientFloat(1.0, -3.7309821933422431,
                                               5.2918958422735862,
                                               -3.3812970922825225,
                                               0.82172646376048009)
        filterMtx = rbbl.BiquadCoefficientMatrixFloat(self.processWidth, 1)
        numberOfInputs = int(self.processWidth/self.width)
        for idx in range(0, numberOfInputs):
            filterMtx[idx, 0] = lfFilter
            filterMtx[idx+numberOfInputs, 0] = hfFilter
        self.filterBank = rcl.BiquadIirFilter(self.context, "filterBank", self,
                                              numberOfChannels=self.processWidth,
                                              numberOfBiquads=1,
                                              initialBiquads=filterMtx,
                                              controlInput=False)
        self.audioConnection(self.input,
                             [i % numberOfInputs for i in range(
                                     0, self.processWidth)],
                             self.filterBank.audioPort("in"),
                             range(0, self.processWidth))


class envelope(visr.AtomicComponent):
    """
    Atomic component that finds the envelope of the given input signal
    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    processWidth: integar
        number of channels post octave band filtering
    bands: ndarray
        array containing the centre frequencies of the octave bands being used
    fTEnvelope: Boolean
        if True the function will feed through audio only

    Output
    ------
    y: 2D array of shape (buffer size, processWidth)
        input signal envelope
    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, processWidth, bands, fTEnvelope):
        super(envelope, self).__init__(context, name, parent)
        # set up the input signal
        self.input = visr.AudioInputFloat("in", self, processWidth)
        # set up the output signal
        self.output = visr.AudioOutputFloat("out", self, processWidth)
        # define fTEnvelope as variables that can be referenced in process
        self.fTEnvelope = fTEnvelope
    """
    ==================
    Define the process
    ==================
    """
    def process(self):
        # input signal
        x = self.input.data()
        # call the script envelopeFunction to finc the envelope of x
        y = envelopeFunction.envelope(x, feedThrough=self.fTEnvelope)
        # set y as the signal flow ouput
        self.output.set(y)


class envelopeCont(visr.AtomicComponent):
    """
    Atomic component that finds the envelope of the given input signal
    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    processWidth: integar
        number of channels post octave band filtering
    bands: ndarray
        array containing the centre frequencies of the octave bands being used

    Hearing aid parameters:
    The following parameters are arrays of floats to be applied to each octave
    band in two channels (length up to 10 for 2 channels and 5 octave bands)
    tauA: ndarry
        envelope attack time
    tauA: ndarry
        envelope release time
    fTEnvelope: Boolean
        if True the function will feed through audio only

    Output
    ------
    y: 2D array of shape (buffer size, processWidth)
        input signal envelope
    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, processWidth, bands, tauA, tauR,
                 fTEnvelope):
        super(envelopeCont, self).__init__(context, name, parent)
        # set up the input signal
        self.input = visr.AudioInputFloat("in", self, processWidth)
        # set up the output signal
        self.output = visr.AudioOutputFloat("out", self, processWidth)
        self.processWidth = processWidth
        self.tauA = tauA
        self.tauR = tauR
        # define fTEnvelope as variables that can be referenced in process
        self.fTEnvelope = fTEnvelope
    """
    ==================
    Define the process
    ==================
    """
    def process(self):
        # input signal
        x = self.input.data()
        # call the script envelopeFunction to finc the envelope of x
        y = envelopeControl.envelope(x, self.samplingFrequency(),
                                     tauA=self.tauA, tauR=self.tauR,
                                     blockSize=self.period(),
                                     processWidth=self.processWidth,
                                     feedThrough=self.fTEnvelope)
        # set y as the signal flow ouput
        self.output.set(y)


class noiseRed(visr.AtomicComponent):
    """
    Atomic component that reduces the noise of the input signal
    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    processWidth: integar
        number of channels post octave band filtering
    fTCompression: Boolean
        if True the function will feed through audio only

    Output
    -------
    y: 2D array of shape (buffer size, processWidth)
        compressed input signal

    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, processWidth, fTNoiseRed):
        super(noiseRed, self).__init__(context, name, parent)
        # set up the input
        self.input = visr.AudioInputFloat(name="in", parent=self,
                                          width=processWidth)
        # set up an input "env" which will take the envelope
        self.env = visr.AudioInputFloat(name="env", parent=self,
                                        width=processWidth)
        # set up the output
        self.output = visr.AudioOutputFloat(name="out", parent=self,
                                            width=processWidth)
        # redefine fTNoiseRed so that it can be referenced in process
        self.fTNoiseRed = fTNoiseRed

    """
    ==================
    Define the process
    ==================
    """
    def process(self):
        # input defined in __init__
        x = self.input.data()
        # envelope defined in __init__
        e = self.env.data()
        # call a function from the script noiseReduction.py that reduces the
        # noise of the input signal x given the envelope
        y = noiseReduction.red(x, self.period(), e,
                               feedThrough=self.fTNoiseRed)
        # set y as the output of the signal flow
        self.output.set(y)


class compression(visr.AtomicComponent):
    """
    Atomic component that finds the envelope of the given input signal
    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    processWidth: integar
        number of channels post octave band filtering

    Hearing aid parameters:
    The following parameters are arrays of floats to be applied to each octave
    band in two channels (length up to 10 for 2 channels and 5 octave bands)
    G: ndarry
        gains
    T: ndarray
        compression thresholds
    CR: ndarry
        compression ratios
    KW: ndarry
        knee widths
    MG: ndarray
        make-up gains
    fTCompression: Boolean
        if True the function will feed through audio only

    Output
    -------
    y: 2D array of shape (buffer size, processWidth)
        compressed input signal

    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, processWidth,
                 T, CR, KW, MG, fTCompression):
        super(compression, self).__init__(context, name, parent)
        # set up the input
        self.input = visr.AudioInputFloat(name="in", parent=self,
                                          width=processWidth)
        # set up an input "env" which will take the envelope
        self.env = visr.AudioInputFloat(name="env", parent=self,
                                        width=processWidth)
        # set up the output
        self.output = visr.AudioOutputFloat(name="out", parent=self,
                                            width=processWidth)
        #  redefine the hearing aid variables using self so they can be
        # referenced in process
        self.T, self.CR, self.KW, self.MG = T, CR, KW, MG
        # redefine fTCompression so that it can be referenced in process
        self.fTCompression = fTCompression
        self.processWidth = processWidth
    """
    ==================
    Define the process
    ==================
    """
    def process(self):
        # input defined in __init__
        x = self.input.data()
        # envelope defined in __init__
        e = self.env.data()
        # call a function from the script compressionFunction.py that
        # compresses the input signal x given the envelope e and the hearing
        # aid parameters
        y = compressionFunctionInterp.compress(x, e, self.T, self.CR,
                                         np.linspace(50, 50, self.processWidth),
                                         np.linspace(90, 90, self.processWidth), 
                                         self.MG, self.processWidth,
                                         feedThrough=self.fTCompression)
        # set y as the output of the signal flow
        self.output.set(y)


class compression2(visr.AtomicComponent):
    """
    Atomic component that finds the envelope of the given input signal
    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    processWidth: integar
        number of channels post octave band filtering

    Hearing aid parameters:
    The following parameters are arrays of floats to be applied to each octave
    band in two channels (length up to 10 for 2 channels and 5 octave bands)
    G: ndarry
        gains
    T: ndarray
        compression thresholds
    CR: ndarry
        compression ratios
    KW: ndarry
        knee widths
    MG: ndarray
        make-up gains
    fTCompression: Boolean
        if True the function will feed through audio only

    Output
    -------
    y: 2D array of shape (buffer size, processWidth)
        compressed input signal

    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, processWidth,
                 T, CR, KW, MG, fTCompression):
        super(compression2, self).__init__(context, name, parent)
        # set up the input
        self.input = visr.AudioInputFloat(name="in", parent=self,
                                          width=processWidth)
        # set up an input "env" which will take the envelope
        self.env = visr.AudioInputFloat(name="env", parent=self,
                                        width=processWidth)
        # set up the output
        self.output = visr.AudioOutputFloat(name="out", parent=self,
                                            width=processWidth)
        #  redefine the hearing aid variables using self so they can be
        # referenced in process
        self.T, self.CR, self.KW, self.MG = T, CR, KW, MG
        # redefine fTCompression so that it can be referenced in process
        self.fTCompression = fTCompression
    """
    ==================
    Define the process
    ==================
    """
    def process(self):
        # input defined in __init__
        x = self.input.data()
        # envelope defined in __init__
        e = self.env.data()
        # call a function from the script compressionFunction.py that
        # compresses the input signal x given the envelope e and the hearing
        # aid parameters
        y = compressionFunction.compress(x, e, self.T, self.CR,
                                         self.KW, self.MG,
                                         feedThrough=self.fTCompression)
        # set y as the output of the signal flow
        self.output.set(y)


class gain(visr.AtomicComponent):
    """
    Atomic component that finds the gain of the given input signal
    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    processWidth: integar
        number of channels post octave band filtering
    G: ndarry
        Array of gains to be applied to each octave
        band in two channels (length up to 10 for 2 channels and 5 octave
        bands)
    fTGain: Boolean
        if True the function will feed through audio only

    Output
    ------
    y: 2D array of shape (buffer size, processWidth)
        input signal with gain applied to the relative channels
    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, processWidth, G, fTGain):
        super(gain, self).__init__(context, name, parent)
        # set up the input signal
        self.input = visr.AudioInputFloat("in", self, processWidth)
        # set up the output signal
        self.output = visr.AudioOutputFloat("out", self, processWidth)
        # redefine gain using sel;f to be referenced in process
        self.G = G
        # redefine fTGain so that it can be referenced in process
        self.fTGain = fTGain
    """
    ==================
    Define the process
    ==================
    """
    def process(self):
        # inpout defined in __init__
        x = self.input.data()
        # call a function from gainFunction.py that applies the gain in the
        # relative bands
        y = gainFunction.calculate(x, self.G, feedThrough=self.fTGain)
        # set y as the output
        self.output.set(y)


class octSum(visr.AtomicComponent):
    """
    Atomic component that performs the octave band summing
    Parameters
    ----------
    context: visr.SignalFlowContext
        visr signal flow context containing the sampling frequency and block
        size
    name: string
        name to give component
    parent: class
        parent class to inherit from
    width: integar
        number of channels to give the ouput
    processWidth: integar
        number of channels the input signal has (width*number of octave bands)
    fTOctSum: Boolean
        if True the function will feed through audio only

    Output
    ------
    y: 2D array of shape (buffer size, width)
        input signal summed across octave bands
    """
    """
    ===============================
    Set up the internal audio ports
    ===============================
    """
    def __init__(self, context, name, parent, width, processWidth, fTOctSum):
        super(octSum, self).__init__(context, name, parent)
        # set up the input signal
        self.input = visr.AudioInputFloat("in", self, processWidth)
        # set up the output signal
        self.output = visr.AudioOutputFloat("out", self, width)
        # redefine width using self to be used in process
        self.width = width
        self.fTOctSum = fTOctSum
    """
    ==================
    Define the process
    ==================
    """
#    def process(self):
#        # input defined in __innit__
#        x = self.input.data()
#        # convert the 2d array into a 3d array of shape (width, buffer size,
#        # number of bands)
#        data3d = x.T.reshape([self.width, x.shape[1],
#                             int(x.shape[0]/self.width)], order='C')
#        # sum data3d across the octave bands to give a 2d array of shape
#        # (width, buffer size)
#        y = np.sum(data3d, axis=2)
#        # set y as the ouput
#        self.output.set(y)
    def process(self):
        # input defined in __innit__
        x = self.input.data()
        # call a function from octaveSum.py that converts the 2d array into a
        # 3d array of shape (width, buffer size, number of bands)
        y = octaveSum.Sum(x, self.width, self.fTOctSum)
        # set y as the ouput
        self.output.set(y)
