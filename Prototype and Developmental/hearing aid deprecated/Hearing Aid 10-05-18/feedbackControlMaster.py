#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Fri May  4 16:13:33 2018

@author: pi
"""
from feedbackControlOffline import offlineHA
import numpy as np
import matplotlib.pylab as plt

fShift = 15

# choose a wavfile to test. 'snarky.wav' is a 2ch 48000 fs file
testFilename = 'snarky'
# perform the offline test
dataFS, t, data, fs = offlineHA(wavName=testFilename,
                                oneBuffer=True,
                                order=2, fShift=fShift,
                                writeName=testFilename+'Shift'+str(
                                        fShift)+'Hz')


dataPlotFS = dataFS.reshape([dataFS.shape[1], dataFS.shape[0]])
dataPlot = data.reshape([data.shape[1], data.shape[0]])
DATAFS = np.fft.fft(dataPlotFS[:, 0])
DATA = np.fft.fft(dataPlot[:, 0])

plt.close('all')
f = np.linspace(0, fs, len(data[1]))
plt.figure()
DATA = np.abs(DATA)
DATAFS = np.abs(DATAFS)
plt.plot(f, DATAFS/np.max(DATAFS), alpha=1,
         label='Notched Signal')
plt.plot(f, DATA/np.max(DATA), alpha=0.7, label='Normal Signal')
plt.legend()
plt.ylabel('Normalised Magnitude')
plt.xlabel('Frequency (hz)')
plt.title('FFT of Data and Notched Data')
plt.xlim([0, fs/2])
