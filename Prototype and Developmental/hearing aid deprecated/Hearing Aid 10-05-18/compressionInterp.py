#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 10 14:23:27 2018

@author: scottvanderleeden
"""
from scipy import interpolate
import numpy as np
from scipy.io import wavfile
import envelopeFunction
import pylab as plt
import envelopeFunction
import envelopeControl
import envelopeFaster

def readwav(wavName):
    """
    """
    # find the relavent hearing aid variables from the band index
    # read the wavfile
    fs, data = wavfile.read(wavName+'.wav')
    # create the correct time vector based on the sampling frequency
    datanew = np.zeros([2, len(data)])
    datanew[0, :] = data
    datanew[1, :] = data
    data = datanew
    return(data, fs)
    
    
testFilename = 'FemaleSpeech'
    # perform the offline test
data, fs = readwav(testFilename)
t = np.arange(0, data.shape[1])/float(fs)

# %%
""" 1 Channel """
tauA = np.array([0.005, 0.005])
tauR = np.array([0.02, 0.02])
#T = np.array([20, 20])
CR = np.array([0.5, 0.5])
KW = np.array([80, 80])
MG = np.array([0, 0])
UK = np.array([60, 60])
LK = np.array([20, 20])
ev = envelopeFaster.envelope(data, fs, tauA, tauR, data.shape[1],
                                  processWidth=2)
#ev = envelopeFunction.envelope(data)

env = ev[0, :]
envdB = np.zeros_like(env)
nonzero = np.where(env != 0)
envdB[nonzero] = 20*np.log10(env[nonzero])
width = 2

maxSig = 200
In = np.array([-5, LK[0], UK[0], maxSig])
Out = np.array([-5, LK[0], CR[0]*LK[0]+(UK[0]-LK[0]), CR[0]*LK[0]+(UK[0]-LK[0])])
f = interpolate.interp1d(In, Out)
InNew = np.linspace(-5, maxSig, len(ev[0, :]))
OutNew = f(InNew)
OutEnv = f(envdB)

G = OutEnv-envdB

g = 10**(G/20)
dataComp = np.multiply(g, data[0, :])
mg = 10**(MG[0]/20)
dataComp = dataComp*mg

plt.close('all')
plt.figure()
plt.plot(In, Out, 'o', label='Knees')
plt.plot(InNew, OutNew, label='Interpolation')
plt.plot(envdB, OutEnv, label='Envelope Input')
plt.xlabel('Input (dB)')
plt.ylabel('Output (dB)')
plt.legend()

plt.figure()
plt.plot(t, data[0, :], label='Data')
plt.plot(t, dataComp, label='Compressed')
plt.plot(t, env, label='Envelope')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude (au)')
plt.legend()
# %%
""" 2 Channel """
tauA = np.array([0.005, 0.005])
tauR = np.array([0.02, 0.02])
#T = np.array([20, 20])
CR = np.array([2, 2])
KW = np.array([80, 80])
MG = np.array([0, 0])
UK = np.array([60, 60])
LK = np.array([20, 20])
ev = envelopeFaster.envelope(data, fs, tauA, tauR, data.shape[1],
                                  processWidth=2)
#ev = envelopeFunction.envelope(data)

env = ev
envdB = np.zeros_like(env)
nonzero = np.where(env != 0)
envdB[nonzero] = 20*np.log10(env[nonzero])
width = 2

maxSig = 200
In = np.array([[-5, -5], LK, UK, [maxSig, maxSig]])
Out = np.array([[-5, -5], LK, np.multiply(CR, LK)+(UK-LK),
                np.multiply(CR, LK)+(UK-LK)])
InNew = np.zeros_like(env)
OutEnv = np.zeros_like(env)
for i in range(width):
    f = interpolate.interp1d(In[:, i], Out[:, i])
    InNew[i, :] = np.linspace(-5, maxSig, len(ev[0, :]))
#    OutNew = f(InNew)
    OutEnv[i, :] = f(envdB[i, :])

G = OutEnv-envdB

g = np.power(10, (G/20))
dataComp = np.multiply(g, data)
mg = np.power(10, (MG/20))
dataComp = dataComp*mg[:, None]
# %%
plt.close('all')
plt.figure()
plt.plot(In[:, 0], Out[:, 0], 'o', label='Knees')
#plt.plot(InNew, OutNew, label='Interpolation')
plt.plot(envdB[0, :], OutEnv[0, :], label='Envelope Input')
plt.xlabel('Input (dB)')
plt.ylabel('Output (dB)')
plt.legend()

plt.figure()
plt.plot(t, data[0, :], label='Data')
plt.plot(t, dataComp[0, :], label='Compressed')
plt.plot(t, env[0, :], label='Envelope')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude (au)')
plt.legend()
