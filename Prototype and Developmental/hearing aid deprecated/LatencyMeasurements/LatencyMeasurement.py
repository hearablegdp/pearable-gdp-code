#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  7 17:30:55 2018

@author: scottvanderleeden
"""
import os
from scipy.io import wavfile


from scipy import signal
import numpy as np
import pylab as plt
import glob
from scipy.stats import linregress

"""
=========================
This script takes latency measurement data where a sine sweep was feed through
the Pi from a seperate computer and also feed through the computer itself.
The cross correleation of the Pi data & input is found. The cross correlation
between the feedthrough computer data & input is found. The delay between
these cross correlations gives the latency.
Different measurements were done for different sampling frequencies (32000,
44100 and 48000) and buffer sizes (512, 1024, 2048 and 4096) with VISR running
a simple feedthrough system.
It was observed that the data for above 32000 Hz sampling frequency was not
giving good results. So these had to be ignored.
See the final group report for more detail on this.
"""


def readwav(wavName, Len):
    fs, x = wavfile.read(wavName)
#    throughPi = np.zeros([x.shape[0], 1])
#    throughMeas = np.zeros([x.shape[0], 1])
    padWidth = Len-len(x)
    throughPi = np.pad(x[:, 0], (0, padWidth), mode='constant')
    throughMeas = np.pad(x[:, 1], (0, padWidth), mode='constant')
    return(throughPi/max(throughPi), throughMeas/max(throughMeas), fs)


def freqBuffer(fileMeas):
    str1 = fileMeas
    freq = np.zeros(len(fileMeas))
    bufferSize = np.zeros(len(fileMeas))
    for n in range(len(fileMeas)):
        str1 = fileMeas[n]
        for i in range(len(str1)):
            if str1[i] == 's':
                if str1[i-5] == 's':
                    if str1[i+1] == 0:
                        bufferSize[n] = (int(str1[i+2]+str1[i+3]+str1[i+4]))
                    else:
                        bufferSize[n] = (int(str1[i+1]+str1[i+2] +
                                             str1[i+3]+str1[i+4]))
                else:
                    freq[n] = (int(str1[i+1]+str1[i+2]+str1[i+3]+'00'))
    return(freq, bufferSize)


def correlate(Input, throughMeas, throughPi):
    Num = len(throughMeas[:, 0])
    corrLen = len(throughMeas[0, :])+len(Input)-1
    corrMeas = np.zeros([Num, corrLen])
    corrPi = np.zeros([Num, corrLen])
    for i in range(Num):
        corrMeas[i, :] = signal.correlate(throughMeas[i, :], Input)
        corrPi[i, :] = signal.correlate(throughPi[i, :], Input)
    return(corrPi, corrMeas)


def calcLatency(corrPi, corrMeas, freq, bufferSize):
    Num = len(corrPi[:, 0])
    Delay = np.zeros(Num)
    Latency = np.zeros(Num)
    bufferLatency = np.zeros(Num)
    for i in range(Num):
        Delay[i] = np.argmax(corrPi[i, :]) - np.argmax(corrMeas[i, :])
        Latency[i] = 1000*Delay[i]/freq[i]
        bufferLatency[i] = 1000*bufferSize[i]/freq[i]
    return(Delay, Latency, bufferLatency)


def master(path):
    os.chdir(path)
    Len = 420000
    files = sorted(glob.glob('*.wav'), key=os.path.basename)
    fileMeas = files[1:]
    # freq = [32000, 32000, 32000, 44100, 44100, 44100, 48000, 48000,
    #        48000, 32000, 32000, 96000]
    # bufferSize = [1024, 2048, 512, 1024, 2048, 512, 1024, 2048, 512,
    #              2000, 4096, 128]
    fs, Input = wavfile.read('00Input.wav')

    throughPi = np.zeros([len(fileMeas), Len])
    throughMeas = np.zeros([len(fileMeas), Len])
    for i in range(len(fileMeas)):
        throughPi[i, :], throughMeas[i, :], _ = readwav(fileMeas[i], Len=Len)

    freq, bufferSize = freqBuffer(fileMeas)
    corrPi, corrMeas = correlate(Input, throughMeas, throughPi)
    Delay, Latency, bufferLatency = calcLatency(corrPi, corrMeas, freq,
                                                bufferSize)
    return(freq, bufferSize, corrPi, corrMeas, Delay, Latency, bufferLatency)


# %%
path = 'FastData'
freqFast, bufferSizeFast, corrPiFast, corrMeasFast,\
 DelayFast, LatencyFast, bufferLatencyFast = master(path)
#
#path = '../SlowData'
#freqSlow, bufferSizeSlow, corrPiSlow, corrMeasSlow,\
# DelaySlow, LatencySlow, bufferLatencySlow = master(path)
#
#path = '../AllData'
#freqAll, bufferSizeAll, corrPiAll, corrMeasAll,\
# DelayAll, LatencyAll, bufferLatencyAll = master(path)
path = '..'
os.chdir(path)

# %%
plt.rcParams['figure.figsize'] = (12., 12.)
plt.rcParams['font.size'] = (12.)
plt.close('all')
position = 7

plt.figure()
plt.plot(corrPiFast[position, :]/np.max(abs(corrPiFast[position, :])), label='Through Pi')
plt.plot(corrMeasFast[position, :]/np.max(abs(corrMeasFast[position, :])), label='Through Meas. System')
plt.title('Fs: '+str(freqFast[position])+' Hz; BufferLatency: '+str(
        np.round(bufferLatencyFast[position]))+' ms' +
          ' Latency: '+str(np.round(LatencyFast[position]))+' ms'+
          ' Hz; Delay: '+str(np.round(DelayFast[position])))
plt.ylabel('Normalised Magnitude')
plt.xlabel('Samples')
plt.legend()
# %%

plt.rcParams['figure.figsize'] = (14., 12.)

font = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 10,
        }
# %%
fig, ax = plt.subplots()
ax.plot(bufferLatencyFast, LatencyFast, 'b.')
ax.set(xlabel='Buffer Latency (ms)', ylabel='Measured Latency (ms)')
m, c, r, p, stdErr = linregress(bufferLatencyFast, LatencyFast)
mn = np.min(bufferLatencyFast)
mx = np.max(bufferLatencyFast)
x1 = np.linspace(mn, mx, 500)
y1 = m*x1+c
ax.plot(x1, y1, '-r')
textstr = 'Estimated hardware latency: %.f ms\nEstimated number of\
 buffers causing latency: %.1f\nSample size: %.f' % (c, m, len(LatencyFast))
props = dict(boxstyle='square', facecolor='white', alpha=0.5)
ax.text(0.05, 0.95, textstr, transform=ax.transAxes,
        verticalalignment='top', bbox=props, fontdict=font)
plt.show()
#
#ax.plot(bufferLatencySlow, LatencySlow, 'b.')
#m, c, r, p, stdErr = linregress(bufferLatencySlow, LatencySlow)
#mn = np.min(bufferLatencySlow)
#mx = np.max(bufferLatencySlow)
#x1 = np.linspace(mn, mx, 500)
#y1 = m*x1+c
#ax.plot(x1, y1, '-r')
#
#print(textstr)
#plt.savefig('LatencytrendFastSlow.png')
# %%
fig, ax = plt.subplots()
ax.plot(bufferLatencyFast, LatencyFast, 'b.')
ax.set(xlabel='Buffer Latency (ms)', ylabel='Measured Latency (ms)')
m, c, r, p, stdErr = linregress(bufferLatencyFast, LatencyFast)
mn = np.min(bufferLatencyFast)
mx = np.max(bufferLatencyFast)
x1 = np.linspace(mn, mx, 500)
y1 = m*x1+c
ax.plot(x1, y1, '-r')
textstr = 'Estimated hardware latency: %.f ms\nEstimated number of\
 buffers causing latency: %.1f\nSample size: %.f' % (c, m, len(LatencyFast))
props = dict(boxstyle='square', facecolor='white', alpha=0.5)
ax.text(0.05, 0.95, textstr, transform=ax.transAxes,
        verticalalignment='top', bbox=props, fontdict=font)
plt.show()
plt.savefig('LatencytrendFast.png')
## %%
#fig, ax = plt.subplots()
#ax.plot(bufferLatencySlow, LatencySlow, 'b.')
#ax.set(xlabel='Buffer Latency (ms)', ylabel='Measured Latency (ms)')
#m, c, r, p, stdErr = linregress(bufferLatencySlow, LatencySlow)
#mn = np.min(bufferLatencySlow)
#mx = np.max(bufferLatencySlow)
#x1 = np.linspace(mn, mx, 500)
#y1 = m*x1+c
#ax.plot(x1, y1, '-r')
#textstr = 'Estimated hardware latency: %.f ms\nEstimated number of\
# buffers causing latency: %.1f\nSample size: %.f' % (c, m, len(LatencySlow))
#props = dict(boxstyle='square', facecolor='white', alpha=0.5)
#ax.text(0.05, 0.95, textstr, transform=ax.transAxes,
#        verticalalignment='top', bbox=props, fontdict=font)
#plt.show()
#
#plt.savefig('LatencytrendSlow.png')
## %%
#fig, ax = plt.subplots()
#ax.plot(bufferLatencyAll, LatencyAll, 'b.')
#ax.set(xlabel='Buffer Latency (ms)', ylabel='Measured Latency (ms)')
#m, c, r, p, stdErr = linregress(bufferLatencyAll, LatencyAll)
#mn = np.min(bufferLatencyAll)
#mx = np.max(bufferLatencyAll)
#x1 = np.linspace(mn, mx, 500)
#y1 = m*x1+c
#ax.plot(x1, y1, '-r')
#textstr = 'Estimated hardware latency: %.f ms\nEstimated number of\
# buffers causing latency: %.1f\nSample size: %.f' % (c, m, len(LatencyAll))
#props = dict(boxstyle='square', facecolor='white', alpha=0.5)
#ax.text(0.05, 0.95, textstr, transform=ax.transAxes,
#        verticalalignment='top', bbox=props, fontdict=font)
#plt.show()
#
#plt.savefig('LatencytrendAll.png')
