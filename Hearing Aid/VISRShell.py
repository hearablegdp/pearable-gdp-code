#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
@author: Scott van der Leeden
"""
# VISR modules:
import visr
import rrl
import audiointerfaces as ai

# scripts written for Pearable GDP:
import VISRComponents
import hearingAidVariables as HA

# other modules:
import time
import numpy as np

"""
========
Overview:
========
This script is the shell that runs the VISR components found in
VISRComponents.py. It is split into three sections:
1. Set up the variables
2. Setup the signal flow
3. Run the signal flow
"""


"""
========================
Part 1: Set up variables
========================
"""
'''SIGNAL PROCESSING VARIABLES'''
# number of audio channels (2 for stereo)
width = 2
# sampling frequency, if using JACK server this must be the same as the Jack
# server is using. This may also be limitted by the sound card's minimum
# sampling frequency
fs = 32000
# block or buffer size, this determines how many samples is sampled at a time
# the 'allowed time'  will be governed by blockSize/fs. This is also the
# software latency
blockSize = 1024
# the order chosen for the band pass filters that split the signal into
# seperate octave bands
bandOrder = 2
# for feedback cancelation the signal will be shifted in the frequency domain
# fShift is the value in Hz to shift the signal up by
fShift = 10
# variable that indexes which bands to use out of the following:
# 250 Hz, 500 Hz, 1000 Hz, 2000 Hz, 4000 Hz.
# This is given to HA.numBands() to get the hearing aid variables


'''BOOLEANS'''
# if feedThrough is true everything will be ignored and a simple atomic
# component feedthrough will be used instead, this can be useful to see if
# your audio system is set up properly
feedThrough = False
# the following Booleans are used to choose whether to ignore the signal
# processing in the functions. If they are True, the function will just
# feedthrough the audio instead of performing the DSP

# Boolean feed through choice for frequency shift
fTFeedbackCont = False
# Boolean feed through choice for octave band filtering
fTBands = True
# Boolean feed through choice for envelope
fTEnvelope = False
# Boolean feed through choice for compression
fTCompression = False
# Boolean feed through choice for noise reduction
fTNoiseRed = True
# Boolean feed through choice for gain
fTGain = False

# Boolean that decides which feedback control to use, if True frequency
# shifting is used if False, notch filtering is used
notchFeedback = False
# Boolean that decides which envelope function to use, if True the hilbert
# transform is used, if False, a custom envelope function is used that allows
# customisable parameters
hilbertEnv = True
# choosed what server to use, if False PortAudio is used, if True JackAudio is
# used
Jack = False


'''HEARING AID PARAMETERS'''
if fTBands:
    bandIndex = np.array([0])
else:
    bandIndex = np.array([1, 2])
# used HA.numbands() to get the hearing aid variables in the right format
# returning centre frequencies of chosen bands bands, gain G, lower knee LK,
# compression ratio CR, upper knee UK and makeup gain MG.
bands, G, LK, CR, UK, MG, tauA, tauR = HA.numBands(bandIndex)
"""
==============================
Part 2: Set up the signal flow
==============================
"""
# set up the sampling frequency and buffer size in VISR
c = visr.SignalFlowContext(blockSize, fs)
if feedThrough:
    pa1 = VISRComponents.feedThrough(context=c, name="FT", parent=None,
                                     width=width)
# runs the composite component Pearable from VISRComponents.py which contains
# the audio connections between the different signal processing classes
# (atomic components).
else:
    pa1 = VISRComponents.Pearable(context=c, name="pa0", parent=None,
                                  width=width, bands=bands,
                                  bandOrder=bandOrder,
                                  G=G, LK=LK, CR=CR, UK=UK, MG=MG,
                                  tauA=tauA, tauR=tauR,
                                  fShift=fShift,
                                  notchFeedback=notchFeedback,
                                  hilbertEnv=hilbertEnv,
                                  fTFeedbackCont=fTFeedbackCont,
                                  fTBands=fTBands,
                                  fTEnvelope=fTEnvelope,
                                  fTCompression=fTCompression,
                                  fTNoiseRed=fTNoiseRed, fTGain=fTGain)


# creates a signal flow class
flow = rrl.AudioSignalFlow(pa1)
# sets up the audio interface config containing the processing functionality
aiConfig = ai.AudioInterface.Configuration(flow.numberOfCaptureChannels,
                                           flow.numberOfPlaybackChannels,
                                           fs,
                                           blockSize)

# if the Jack config is chosen.
# NOTE: the Jack config will need to be ran externally
if Jack:
    # sets up the Jack config
    jackCfg = """{ "clientname": "Pearable2",
      "autoconnect" : "true",
      "portconfig":
      {
        "capture":  [{ "basename":"in_", "externalport" : {} }],
        "playback": [{ "basename":"out_", "externalport" : {} }]
      }
    }
    """
    # creates the audio factory containing all of the parameters needed for the
    # signal flow
    aIfc = ai.AudioInterfaceFactory.create("Jack", aiConfig, jackCfg)
# if Jack is not chosen use PortAudio instead
# NOTE: This does not need to be ran externally
else:
    # creates the audio factory containing all of the parameters needed for the
    # signal flow
    aIfc = ai.AudioInterfaceFactory.create("PortAudio", aiConfig,
                                           """{"hostapi":"ALSA"}""")

# sets up the signal flow with the Pearable composite component
aIfc.registerCallback(flow)

"""
=================================
Part 3: Run the audio signal flow
=================================
"""
# start a timer
start_time = time.time()
# start the signal flow
aIfc.start()
# print the allowed processing time
print("Allowed Time {} ms".format(1000*blockSize/(float(fs))))
# prints how long it took to start the signal flow
print("Rendering started, aIfc start time:. {}".format(time.time()-start_time))

# wait five seconds
time.sleep(5)
# print this
i = input("Enter text (or Enter to quit): ")
if not i:
    aIfc.stop()
    aIfc.unregisterCallback()
    del aIfc
