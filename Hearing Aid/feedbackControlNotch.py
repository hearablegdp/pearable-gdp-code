# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
# this imports the scipy function _design_notch_peak_filter which has been
# copied into scipyNotchPeakFilter.py. This is only available for scipy 0.19.0
# which cant be installed on the Pi yet.
import scipyNotchPeakFilter as SNPF
import scipy.signal as signal


def notch(x, width, blockSize, fs, thresh=5, taps=5, bw=60,
          feedThrough=False):
    """
    Checks if a peak in the frequency domain is too high in amplitude, if it is
    then notch filter it out and convert back to the time domain
    Parameters
    ----------
    x: ndarray
        the data to be shifted
    width: integar
        the number of channels of the input signal
    blockSize: integar
        the length of the data (best to be a power of 2)
    fs: integar
        sampling frequency of the data
    fShift: integar
        the frequency to shift the up by, this will be approximate as the exact
        frequncy shift is limited to sampling limitaitons
    feedThrough: Boolean
        if True feed through audio only

    Returns
    -------
    xShift: ndarray
        the time domain, frequency shifted data
    """
    # if feedThrough is True return the input as the output
    if feedThrough:
        return(x)
    X = np.fft.rfft(x)
    Y = np.zeros_like(X)
    XAbs = np.abs(X)
    # create a tuple index to find the max in each channel
    # index the y axis (an index for each channel)
    maxIndxY = np.arange(width)
    # index the x axis (the maximum of each channel)
    maxIndxX = np.argmax(XAbs, axis=1)
    # combine the y and x indecies to create a tuple index for the max of each
    # data channel
    maxIndx = maxIndxY, maxIndxX
    # find the maximum of each channel
    maxX = XAbs[maxIndx]
    # define a limit relative to XAbs that will be used to assess whether to
    # notch filter. np.std() finds the standard deviation of the signal
    peakLimit = np.mean(XAbs) + thresh*np.std(XAbs)
    # if there is a large amplitude peak in the fft for any channel, notch
    # filter it out
    if (maxX > peakLimit).any():
        # find the frequency of the maximum peak
        f = fs*(np.array(maxIndx[1])/blockSize)
        # loop through each channel to apply the notch filter
        for i in range(len(f)):
            # normalise the frequency
            w0 = f[i]/(0.5*fs)
            # quality factor, determines the width of the notch
            Q = w0/bw
            # apply the notch filter using
            # scipy.signal._design_notch_peak_filter whcih has been copied to
            # scipyNotchPeakFilter.py
            b, a = SNPF._design_notch_peak_filter(w0, Q, 'notch')
            # find the FRF of the notch filter
            _, H = signal.freqz(b, a, worN=blockSize)
            # apply the filter through frequency domain multiplication
            Y[i, :] = Y[i, :]*np.abs(H)
    # convert to the frequency domain
    y = np.fft.irfft(Y)
    # output the notch filtered signal y
    return y
