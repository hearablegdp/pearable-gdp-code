#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Wed Apr 11 11:51:01 2018

@author: pi
"""

import numpy as np
"""
The following variables are set for all octave bands for left and right
channels. The first five correspond to the left channel and next five, the
right channel.
================================================================
This script is where the hearing aid parameters can be adjusted.
================================================================
The script VISRShell.py uses the below function numBands() to index the octave
bands that have been chosen here.
"""
# octave band center frequencies
bandsDef = np.array([250, 500, 1000, 2000, 4000])
# Gain
GDef = np.array([1., 1., 1., 1., 1., 1., 1., 1., 1., 1.])
# Lower knee
LKDef = np.array([50., 50., 50., 50., 50., 50., 50., 50., 50., 50.])
# Compression ratio
CRDef = np.array([2., 2., 2., 2., 2., 2., 2., 2., 2., 2.])
# Upper knee
UKDef = np.array([80., 80., 80., 80., 80., 80., 80., 80., 80., 80.])
# Makeup gain
MGDef = np.array([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.])
# attack time
tauA = np.array([0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005,
                 0.005])
# release time
tauR = np.array([0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02,
                 0.02])


def numBands(indxBands, bandsDef=bandsDef, GDef=GDef, LKDef=LKDef,
             CRDef=CRDef, UKDef=UKDef, MGDef=MGDef, tauA=tauA, tauR=tauR):
    """
    Converts the variables for all channels (5 octave bands for left and right
    ear) into the right number of channels based on the band index provided
    Parameters
    ----------
    indxBands: ndarry
        an index of the bands to be used this will index the bands given
        bandsDef
    bandsDef: ndarry
        array of the octave band centre frequencies to be indexed from
    GDef: ndarry
        gains
    LKDef: ndarray
        lower knees
    CRDef: ndarry
        compression ratios
    UKDef: ndarry
        upper knees
    MGDef: ndarray
        make-up gains

    Returns
    -------
    Correct arrays corresponding to the octave bands chosen
    bands: ndarray
        octave band centre frequencies chosen
    G: ndarry
        gains
    LK: ndarray
        lower knees
    CR: ndarry
        compression ratios
    UK: ndarry
        upper knees
    MG: ndarray
        make-up gains
    """
    # create correct array of octave band centre frequencies
    bands = bandsDef[indxBands]
    # create an index that applies to both right and left channels
    indxLR = np.concatenate((indxBands, indxBands+len(bandsDef)))
    # index the Threshold
    LK = LKDef[indxLR]
    # index the Compression Ratio
    CR = CRDef[indxLR]
    # index the Knew Width
    UK = UKDef[indxLR]
    # index the Make-up Gain
    MG = MGDef[indxLR]
    # index the Gain
    G = GDef[indxLR]
    # index the attack time
    tauA = tauA[indxLR]
    # index the release time
    tauR = tauR[indxLR]
    return(bands, G, LK, CR, UK, MG, tauA, tauR)
