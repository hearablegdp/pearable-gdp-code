#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Mon Apr  9 12:30:45 2018

@author: pi
"""

import numpy as np
from scipy import signal


def getFilterCoeffs(fs, bands, order):
    """
    Creates filter coefficients for each octave band filter.
    Creates a chain of band pass filters.
    The lowest octave band is a low pass filter only.
    The highest octave band is a high pass filter only.

    Parameters
    ----------
    fs: integar
        sampling frequency
    bands: ndarray
        centre frequencies of the octave bands being used
    order: integar
        order of the filters to be used for the band pass filters
        for the high and low pass filters this order is doubled to get the
        same number of filter coefficients for all types of filters

    Returns
    -------
    b, a : ndarray, ndarray; shape (number of bands, 2*order+1)
        Numerator (``b``) and denominator (``a``) polynomials
        of the IIR filter.
    """
    # a scalar used to find the upper and lower octave band frequencies
    fD = 2**0.5
    # find the upper and lower frequencies using fD
    fU, fL = bands*fD, bands/fD
    # normalises these frequencies and puts into one array
    Wn = np.array([fL, fU])/(0.5*fs)
    # creates empty arrays for the a and b coefficients for each channel
    b = np.zeros([len(bands), order*2+1])
    a = np.zeros([len(bands), order*2+1])
    # if only one band is given then band pass filter it
    if len(bands) == 1:
        b, a = signal.butter(order, Wn, btype='band', output='ba')
    # if multiple bands are given:
    else:
        for i in range((len(bands))):
            # for the lowest band low pass filter instead
            if i == 0:
                b[i, :], a[i, :] = signal.butter(order*2, Wn[:, i][1],
                                                 btype='low', output='ba')
            # for the highest band high pass filter instead
            elif i == (len(bands)-1):
                b[i, :], a[i, :] = signal.butter(order*2, Wn[:, i][0],
                                                 btype='high', output='ba')
            # for all other bands band pass filter
            else:
                b[i, :], a[i, :] = signal.butter(order, Wn[:, i],
                                                 btype='band', output='ba')
    # NOTE: to ensure that the a and b coefficients are a consistent length,
    # the order of the low and high pass filters is double that of the band
    # pass filters. This is because a band pass filter performs both low and
    # high pass filtering so is double the length.
    # return the coefficients
    return (b, a)


def applyFilterCoeffs(x, b, a, width, bufferSize, bands, feedThrough=False):
    """
    Applies a chain of filters given by a and b.

    Parameters
    ----------
    x: ndarry
        signal to be filtered
    b: ndarry
        b coefficients
    a: ndarry
        a coefficients
    width: integar
        number of channels x has
    bufferSize: integar
        length of the signal x (size of the processing buffer)
    bands: ndarry
        centre frequencies of the octave bands
    feedThrough: Boolean
        if True feed through audio only

    Returns
    -------
    y: ndarray shape (width*number of bands, buffer size)
        filtered signal
    """
    # if feedThrough is True return the input as the output
    if feedThrough:
        return(x)
    # create an empty 3D array to assign y to of shape (x channels, buffer
    # size, number of bands)
    y = np.zeros([width, bufferSize, len(bands)])
    # if only one band is chosen
    if len(bands) == 1:
        # apply the coefficients as normal
        y = signal.lfilter(b, a, x)
    # if multiple bands are chosen
    else:
        # apply the coefficients to each band in order using a for loop
        for i in range(len(bands)):
            y[:, :, i] = signal.lfilter(b[i, :], a[i, :], x)
    # reshape the 3D array y to be 2D of shape (x channels* number of bands)
    y = y.T.reshape([x.shape[0]*len(bands), x.shape[1]], order='A')
    return(y)
