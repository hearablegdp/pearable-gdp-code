# -*- coding: utf-8 -*-
"""
Created on Sun Nov 12 20:00:49 2017

@author: Owner
"""

import adaptfilt as af
import numpy as np
import matplotlib.pyplot as plt


#y, e, w = af.lms(u, d, M, step, leak=0, initCoeffs=None, N=None, reurnCoeffs=False )

"""
u: the one dimensionl filter input
d: one dimensional desired signal
M: the desired number of filter taps (order + 1)
step: step size of the algorithm

optional parameters:
leak: when greater than one a leaky LMS algorithm is used
initCoeffs: Initial filter coefficients to use.
N: number of iterations
returnCoeffs: will return all filter coefficients for every iteration

Returns
y: output values of LMS filter array
e: error signal
w: filter coefficients in array

"""

def sine(f=44.0, fs=44100., duration=5.0, plot=True):
    """
    Added plot boolean as the plots take time on the pi, to improve speed
    of functions without generating plots
    """
    samples = (np.sin(2*np.pi*np.arange(fs*duration)*f/fs)).astype(np.float32)
    time = np.arange(0, duration, 1/fs)
    noise = np.random.normal(0, duration, int(fs*duration))
    sig = samples + noise
    if plot:
        plt.plot(time, samples)
        plt.figure()
        plt.plot(time, noise)
        plt.figure()
        plt.plot(time, sig)
        plt.show()    
    return samples, sig


def LMS(plot=True):
    samples, sig = sine(plot=plot)
    y, e, w = af.lms(sig, samples, 1, 0.01)
    if plot:
        plt.plot(y)
	return y, e, w
 
LMS()
