# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt


def to_complex(field):
	return complex(field.replace(' ', '').replace('+-', '-').replace('i', 'j'))


# read in plant transfer functions from txt file
H1_plant = np.loadtxt("32 1 plant.txt",converters={0: to_complex}, dtype=np.complex128)
H2_plant = np.loadtxt("32 2 plant.txt",converters={0: to_complex}, dtype=np.complex128)
H3_plant = np.loadtxt("32 3 plant.txt",converters={0: to_complex}, dtype=np.complex128)


H2_plant_f = np.loadtxt("32 2 plant freqs.txt")


# find the indices of less than 1000 Hz
indices = np.where(H2_plant_f < 1000)
cutoff = indices[-1]
def main():
	plt.figure()
	plt.plot(H2_plant_f[0:cutoff[-1]], H2_plant[0:cutoff[-1]])
	plt.show()


def fxlms(x_hat2, period_size): #, FRF=H2):
	
	# obtain FRF data up to 1000 Hz
	FRF = H2_plant[0:2048]
	
	signal_fft = np.fft.fft(x_hat2, n=2048)		 	# obtain an FFT of the signal
	FRF_mult = FRF*signal_fft			 				# multiply the two together
	signal_IFFT = np.fft.irfft(FRF_mult, n=1024)  # find the inverse
	
	return signal_IFFT


