# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt


def to_complex(field):
	return complex(field.replace(' ', '').replace('+-', '-').replace('i', 'j'))


# read in transfer upstream functions from txt file
H1 = np.loadtxt("1 Upstream.txt",converters={0: to_complex}, dtype=np.complex128)
H2 = np.loadtxt("2 Upstream.txt",converters={0: to_complex}, dtype=np.complex128)
H3 = np.loadtxt("3 Upstream.txt",converters={0: to_complex}, dtype=np.complex128)

H2_freqs = np.loadtxt("2 Upstream freqs.txt")

# find the indices of less than 1000 Hz
indices = np.where(H2_freqs < 1000)
cutoff = indices[-1]
def main():
	plt.figure()
	plt.plot(20*np.log10(H2[0:2048]))
	plt.show()


def feedbk_cnt(u_n, period_size):# FRF=H2, cutoff=2048):
	
	# obtain FRF data up to 1000 Hz
	FRF = H2[0:2048]
	
	# FFT u_n and apply FRF
	signal_fft = np.fft.fft(u_n, n=2048)
	FRF_mult = FRF*signal_fft
	signal_IFFT = np.fft.irfft(FRF_mult, n=1024)

	return signal_IFFT

if __name__ == '__main__':
        main()
