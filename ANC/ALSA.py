# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pyaudio
# import wave, utils

BUFFER_SIZE = 1024
REC_SECONDS = 5
RATE = 44100
# WAV_FILENAME = utils.generate_random_token()
# FORMAT = pyaudio.paInt16
FORMAT = 8

# init sound stream
pa = pyaudio.PyAudio()
stream = pa.open(
    format=FORMAT,
    input=True,
    channels=1,
    rate=RATE,
    input_device_index=7,
    frames_per_buffer=BUFFER_SIZE
)

# run recording
print('Recording...')
data_frames = []
for f in range(0, RATE/BUFFER_SIZE * REC_SECONDS):
    data = stream.read(BUFFER_SIZE)
    data_frames.append(data)
print('Finished recording...')
stream.stop_stream()
stream.close()
pa.terminate()
