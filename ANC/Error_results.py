# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import matplotlib.pyplot as plt
import numpy as np


def main():
	# read in NLMS output without feedback control
	no_ANC = np.loadtxt('Error output no ANC.txt')
	
	ANC = np.loadtxt('Error output ANC.txt')


	# plot of the FFT
	plt.figure()
	plt.plot(no_ANC[0:6000], label='No ANC')
	plt.plot(ANC[0:6000], label='ANC')
	plt.legend()
	plt.xlabel('Samples')
	plt.ylabel('Amplitude')
	plt.savefig('Result NLMS')
	plt.show()



if __name__ == '__main__':
        main()
