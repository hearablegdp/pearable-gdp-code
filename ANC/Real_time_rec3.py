# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import alsaaudio
import sys
import time
from scipy.io import wavfile
import numpy as np
from numpy import reshape
import matplotlib.pyplot as plt
import wave
import padasip as pad
import scipy.signal as signal
from copy import deepcopy

# PCM stands for Pulse Code Modulation. It is the standard form of digital audio in computers. IN a PCM stream the amplitude of the
# analogue signal is sampled at reqular intervals and each sample is quantized to the nearest value within a range of digital steps.

# audio variables
channels = 2                        # number of channels
sample_size = 2                     # number of analog samples in bytes
frame_size = channels*sample_size   # one sample being played irrespective of channels or bits
									# for stereo and S16_LE: 2*(16 bits) = 4 bytes per sample
sample_rate = 44100					# sample rate in Hz
bps_rate = sample_size*sample_rate  # bytes per second data required to sustain the system. =channels*sample_size*analog_rate
									# = frame_size*analog_rate
bps_rate = sample_rate*frame_size    # the number of bytes that are processed per unit of time.
period_size = 512                  # The number of frames in between each hardware interrupt.
									# Currently this will create an interrupt every 0.02s


###### This is from the editing from the asound.config file 
#device = PCM.plug_onboard_left

# set output to playback audio. PCM_normal mode blocks until a full period is available and then returns a tuple (length,data)
# where length is the number of frames of captured data, and data is the capture sound frames as a string.
# length of data = periodsize*framesize in bytes

###### set input device parameters
inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, 'hw:0,0')
inp.setchannels(2)
inp.setrate(sample_rate)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
inp.setperiodsize(period_size)

### from editing the config files, 

###### set output device parameters

out = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, alsaaudio.PCM_NORMAL, 'hw:0,0')
out.setchannels(2)
out.setrate(sample_rate)
out.setformat(alsaaudio.PCM_FORMAT_S16_LE)
out.setperiodsize(period_size)

##### Filter parameters #####

n = 3		 # filter length (number of taps)
mu = 0.0001  # step size


###### To LMS #####
#ref_est = np.zeros(period_size/2)		# empty vector for filter output
#coeffs = np.zeros((period_size/2, n)) 	# empty vector for filter coefficients
coeffs = []

## design the FIR firwin filter	initial coefficients										
#coeffs[0, :] = 
#coeffs[0] = signal.firwin(n, cutoff=0.3, window="hamming")




def main():
	
	graph = []
	inpu = np.zeros((200, period_size))
	
	
	###### To LMS #####
	#ref_est = np.zeros(period_size/2)		# empty vector for filter output
	#coeffs = np.zeros((period_size/2, n)) 	# empty vector for filter coefficients

	## design the FIR firwin filter	initial coefficients										
	#coeffs[0, :] = signal.firwin(n, cutoff=0.3, window="hamming")


	count = 0

	while count <200:

		l, data = inp.read()	# read data and length of buffer

		if l == period_size:    # check that the data collection is equal to the period_size
			num = np.fromstring(data, dtype='int16')    # convert to numerical data

			# split left and right input channels				
			#right = deepcopy(num)	# copy array
			#right[0::2] = 0			# set left samples to zero
			
			r = deepcopy(num)
			right = r[1::2]
			
		#	left = deepcopy(num)	# copy array
		#	left[1::2] = 0			# set right samples to zero
			
			l = deepcopy(num)
			left = l[0::2]
			inpu[count, :] = left
			
			 #signals creation: u, v, d
		
			
			## convert to a matrix input
			x = pad.input_from_history(left, n, bias=False)
		#	x = pa.input_from_history(d, n)[:-1]
			print(x)
			
			## window error signal
			error = pad.input_from_history(right, (n-1), bias=True)
			
			filt = pad.filters.FilterLMS(n, mu)

			d = left[(n-1):]
			print(len(d))
			print(len(x))

			y, e, w = filt.run(d, x)
			graph.append(y)
			

			
			#for i in range(500):
				
				#y = filt.predict(x[i, :])
			
				#graph.append(y)

			
			# add 0 back into array
			#ref_est = ref_est.reshape((len(ref_est)/2, 2))
			#ref_est = np.insert(ref_est, (0, 1), 0, axis=1)
			#ref_est = ref_est.flatten()

		#	print(ref_est)
			
			# write out data to loudspeaker
			data_proc = np.array(num, dtype=np.int16)		# convert back to alsa data
			out.write(data_proc)						# output data
			count += 1
			#print(ref_est)
		#	graph.append(ref_est)
			#plot.append(np.concatenate(ref_est))

#for i in range(len(inpu)):
	#while i < len(inpu):
		#np.concatenate(i, i+1)
	p = inpu.ravel()
	print(inpu[0, :])
	print(p[0:512])
# plot of output
	plt.figure()
	plt.plot(graph)
	plt.plot(p)
	plt.show()
		#samples = np.arange((i*period_size), (period_size*(i+1)))
		#plt.plot(samples, graph[i])		

##### Link to ANC trial

# signal from mic is now the error source.
# does the filter act on each period and treat it like one sample? - probably as too slow otherwise.
# LMS will filter the audio signal and then create a signal similar to the one captured.
# This signal will be sent to the loudspeaker and written out. - might need a delay at the start before the filter starts working.
# error signal is captured from the error mic abd input into the LMS algorithm.


if __name__ == '__main__':
        main()
