"""
This script records from the reference microphone for a number
of times and then writes the output to a wavfile.
"""


import alsaaudio
from scipy.io import wavfile
import numpy as np
from numpy import reshape
import matplotlib.pyplot as plt
import scipy.signal as signal
from copy import deepcopy


# Set audio variables
channels = 2                        # number of channels
sample_size = 2                     # number of analog samples in bytes
fs = 32000							# sample rate in Hz
period_size = 512                   # The number of frames in between each hardware interrupt.


# Set input device parameters
inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, 'hw:0,0')
inp.setchannels(2)
inp.setrate(fs)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
inp.setperiodsize(period_size)


# Set output device parameters
out = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, alsaaudio.PCM_NORMAL, 'hw:0,0')
out.setchannels(2)
out.setrate(fs)
out.setformat(alsaaudio.PCM_FORMAT_S16_LE)
out.setperiodsize(period_size)

itera = 625

def main():
	
	count = 0
	mic_out = np.zeros((itera, period_size))				# empty matrix for mic data

	while count < itera - 1:

		l, data = inp.read()			# read mic data and length of buffer
		if l == period_size:    		# check that the data collection is equal to the period_size

		    # convert mic data to numerical data	
			num = np.fromstring(data, dtype='int16')

			# Split left and right input channels and obtain only error mic
			r = deepcopy(num)
			right = r[1::2]
			mic_out[count, :] = right
			print(count)
			count += 1
	
	# re-arrange signals into a vector
	mic_sig = mic_out.ravel()
	
	plt.figure()
	plt.plot(mic_sig)
	plt.show()
	
	# write to a wavfile
	wavfile.write('32_Measured_output_upstream_T3.wav', fs, mic_sig)
	# Scale and write to a wavfile
	scaled = np.int16(mic_sig/np.max(np.abs(mic_sig))*32767)
	wavfile.write('32_Measured_scaled_output_upstream_T3.wav', fs, scaled)

if __name__ == '__main__':
        main()
