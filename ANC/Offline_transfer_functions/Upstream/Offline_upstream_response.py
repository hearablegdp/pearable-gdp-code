"""
This script calculates the transfer function for the upstream response
by reading in wavfiles containing the input pink noise to the secondary
source and the output signals measured at the reference mic. Three
measurements were made and the Transfer function, coherence and impulse
response of each are plotted.
"""

from scipy.io import wavfile
import numpy as np
from numpy import reshape
import matplotlib.pyplot as plt
import scipy.signal as signal


fs = 32000

def main():
	
	# read in pink noise: 44.1 kHz, 16 bit, mono.
	fsp, pink = wavfile.read('32 Pink noise 5 seconds.wav')

	# read in measured output: 44.1 kHz, 16 bit, mono.
	fso, mic1 = wavfile.read('32_Measured_output_upstream_T1.wav')
	fso, mic2 = wavfile.read('32_Measured_output_upstream_T2.wav')
	fso, mic3 = wavfile.read('32_Measured_output_upstream_T3.wav')

	# cut measured mic signals to capture only the mic signal
	t1 = mic1[37184:197184]
	t2 = mic2[42443:202443]
	t3 = mic3[96970:256970]

	mics = np.array([t1, t2, t2])
	
	# write mic signals to a wavfile
	wavfile.write('32_exact_measured_output_upstream_T1.wav', fs, t1)
	wavfile.write('32_exact_measured_output_upstream_T2.wav', fs, t2)
	wavfile.write('32_exact_measured_output_upstream_T3.wav', fs, t3)
	
	# compute transfer function from PSD and CPSD
	H = []
	for i in range(len(mics)):
		f1, Pyx = signal.csd(mics[i], pink, fs, nperseg=(2**16))
		f2, Pxx= signal.welch(pink, fs, nperseg=(2**16))
		Hs = Pyx/Pxx
		print(np.array(Hs))
		print(Hs.shape)
		print(Hs.dtype)
		H.append(Hs)
		num = i+1
		# write data out to a txt file
		np.savetxt('{}'.format(i+1)+' Upstream.txt', Hs, delimiter=',')
		np.savetxt('{}'.format(i+1)+' Upstream freqs.txt', f1, delimiter=',')

	
	# Plot of transfer function, coherence and IR for T1
	plt.figure()
	plt.subplot(221)
	plt.semilogx(f1, 20*np.log10(np.abs(H[0])))
	plt.xlim(0, 1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Magnitude (dB/Hz)')
	plt.title('Transfer function of H Magnitude')
	
	plt.subplot(222)
	H_phase = np.rad2deg(np.unwrap(np.angle(H[0], deg=False)))
	plt.semilogx(f1, H_phase)
	plt.xlim(0, 1000)
	plt.ylim(-1000,1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Phase (degrees)')
	plt.title('Transfer function of H phase')

	plt.subplot(223)
	coherence, f = plt.cohere(pink, t1, 2**16, fs)
	plt.xlim(0, 1000)
	plt.ylabel('Coherence')
	plt.xlabel('Samples')
	plt.title('Coherence of the input and output signals')

	IR = np.fft.ifft(H[0])
	plt.subplot(224)
	plt.plot(IR[0:len(IR)/2])
	plt.xlabel('Samples')
	plt.ylabel('Amplitude')
	plt.title('Impulse response')
	plt.tight_layout()
	plt.savefig('Transfer H T1.png')
	plt.show()


	# Plot of transfer function, coherence and IR for T2
	plt.figure()
	plt.subplot(221)
	plt.semilogx(f1, 20*np.log10(np.abs(H[1])))
	plt.xlim(0, 1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Magnitude (dB/Hz)')
	plt.title('Transfer function of H Magnitude')
	
	plt.subplot(222)
	H_phase = np.rad2deg(np.unwrap(np.angle(H[1], deg=False)))
	plt.semilogx(f1, H_phase)
	plt.xlim(0, 1000)
	plt.ylim(-1000,1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Phase (degrees)')
	plt.title('Transfer function of H phase')

	plt.subplot(223)
	coherence, f = plt.cohere(pink, t2, 2**16, fs)
	plt.xlim(0, 1000)
	plt.ylabel('Coherence')
	plt.xlabel('Samples')
	plt.title('Coherence of the input and output signals')

	IR = np.fft.ifft(H[1])
	plt.subplot(224)
	plt.plot(IR[0:len(IR)/2])
	plt.xlabel('Samples')
	plt.ylabel('Amplitude')
	plt.title('Impulse response')
	plt.tight_layout()
	plt.savefig('Transfer H T2.png')
	plt.show()


	# Plot of transfer function, coherence and IR for T3
	plt.figure()
	plt.subplot(221)
	plt.semilogx(f1, 20*np.log10(np.abs(H[2])))
	plt.xlim(0, 1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Magnitude (dB/Hz)')
	plt.title('Transfer function of H Magnitude')
	
	plt.subplot(222)
	H_phase = np.rad2deg(np.unwrap(np.angle(H[2], deg=False)))
	plt.semilogx(f1, H_phase)
	plt.xlim(0, 1000)
	plt.ylim(-1000,1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Phase (degrees)')
	plt.title('Transfer function of H phase')

	plt.subplot(223)
	coherence, f = plt.cohere(pink, t3, 2**16, fs)
	plt.xlim(0, 1000)
	plt.ylabel('Coherence')
	plt.xlabel('Samples')
	plt.title('Coherence of the input and output signals')

	IR = np.fft.ifft(H[2])
	plt.subplot(224)
	plt.plot(IR[0:len(IR)/2])
	plt.xlabel('Samples')
	plt.ylabel('Amplitude')
	plt.title('Impulse response')
	plt.tight_layout()
	plt.savefig('Transfer H T3.png')
	plt.show()
	
	

if __name__ == '__main__':
        main()
