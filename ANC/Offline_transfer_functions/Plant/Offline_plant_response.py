"""
This script calculates the transfer function for the plant response by
reading in wavfiles containing the input pink noise to the secondary
source and the output signals measured at the error mic. Three
measurements were made and the Transfer function, coherence and impulse
response of each are plotted.
"""

from scipy.io import wavfile
import numpy as np
from numpy import reshape
import matplotlib.pyplot as plt
import scipy.signal as signal


fs = 32000

def main():
	
	# read in pink noise: 32 kHz, 32 bit, mono.
	fsp, pink = wavfile.read('32 Pink noise 5 seconds.wav')

	# read in measured output: 32 kHz, 16 bit, mono.
	fso, mic1 = wavfile.read('32_Measured_output_T1.wav')
	fso, mic2 = wavfile.read('32_Measured_output_T2.wav')
	fso, mic3 = wavfile.read('32_Measured_output_T3.wav')

	# cut measured mic signals to capture only the mic signal
	t1 = mic1[59960:219960]
	t2 = mic2[58686:218686]
	t3 = mic3[55521:215521]
	
	mics = np.array([t1, t2, t2])
	
	# write mic signals to a wavfile
	wavfile.write('32_exact_measured_output_T1.wav', fs, t1)
	wavfile.write('32_exact_measured_output_T2.wav', fs, t2)
	wavfile.write('32_exact_measured_output_T3.wav', fs, t3)
	
	# compute transfer function from PSD and CPSD
	G = []
	for i in range(len(mics)):
		f1, Pyx = signal.csd(mics[i], pink, fs, nperseg=2**16)
		f2, Pxx= signal.welch(pink, fs, nperseg=2**16)
		Gs = Pyx/Pxx
		G.append(Gs)
		np.savetxt('32 {}'.format(i+1)+' plant.txt', Gs)
		np.savetxt('32 {}'.format(i+1)+' plant freqs.txt', f1)
	
	# Plot of transfer function, coherence and IR for T1
	plt.figure()
	plt.subplot(221)
	plt.semilogx(f1, 20*np.log10(np.abs(G[0])), label='T1')
	plt.xlim(0, 1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Magnitude (dB/Hz)')
	plt.title('Transfer function of G Magnitude')
	
	plt.subplot(222)
	G0_phase = np.rad2deg(np.unwrap(np.angle(G[0], deg=False)))
	plt.semilogx(f1, G0_phase, label='T1')
	plt.xlim(0, 1000)
	plt.ylim(-1000,1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Phase (degrees)')
	plt.title('Transfer function of G phase')

	plt.subplot(223)
	coherence, f = plt.cohere(pink, t1, 2**16, fs)
#	plt.xlim(0, 1000)
	plt.ylabel('Coherence')
	plt.xlabel('Frequency')
	plt.title('Coherence of the input and output signals')

	IR = np.fft.ifft(G[0])
	plt.subplot(224)
	plt.plot(IR[0:len(IR)/2])
	plt.xlabel('Samples')
	plt.ylabel('Amplitude')
	plt.title('Impulse response')
	plt.tight_layout()
	plt.savefig('Transfer G T1.png')
	plt.show()


	# Plot of transfer function, coherence and IR for T2
	plt.figure()
	plt.subplot(221)
	plt.semilogx(f1, 20*np.log10(np.abs(G[1])))
	plt.xlim(0, 1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Magnitude (dB/Hz)')
	plt.title('Transfer function of G Magnitude')
	
	plt.subplot(222)
	G_phase = np.rad2deg(np.unwrap(np.angle(G[1], deg=False)))
	plt.semilogx(f1, G_phase)
	plt.xlim(0, 1000)
	plt.ylim(-1000,1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Phase (degrees)')
	plt.title('Transfer function of G phase')

	plt.subplot(223)
	coherence, f = plt.cohere(pink, t2, 2**16, fs)
	plt.xlim(0, 1000)
	plt.ylabel('Coherence')
	plt.xlabel('Samples')
	plt.title('Coherence of the input and output signals')

	IR = np.fft.ifft(G[1])
	plt.subplot(224)
	plt.plot(IR[0:len(IR)/2])
	plt.xlabel('Samples')
	plt.ylabel('Amplitude')
	plt.title('Impulse response')
	plt.tight_layout()
	plt.savefig('Transfer G T2.png')
	plt.show()


	# Plot of transfer function, coherence and IR for T3
	plt.figure()
	plt.subplot(221)
	plt.semilogx(f1, 20*np.log10(np.abs(G[2])))
	plt.xlim(0, 1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Magnitude (dB/Hz)')
	plt.title('Transfer function of G Magnitude')
	
	plt.subplot(222)
	G0_phase = np.rad2deg(np.unwrap(np.angle(G[2], deg=False)))
	plt.semilogx(f1, G0_phase)
	plt.xlim(0, 1000)
	plt.ylim(-1000,1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Phase (degrees)')
	plt.title('Transfer function of G phase')

	plt.subplot(223)
	coherence, f = plt.cohere(pink, t3, 2**16, fs)
	plt.xlim(0, 1000)
	plt.ylabel('Coherence')
	plt.xlabel('Samples')
	plt.title('Coherence of the input and output signals')

	IR = np.fft.ifft(G[2])
	plt.subplot(224)
	plt.plot(IR[0:len(IR)/2])
	plt.xlabel('Samples')
	plt.ylabel('Amplitude')
	plt.title('Impulse response')
	plt.tight_layout()
	plt.savefig('Transfer G T3.png')
	plt.show()


if __name__ == '__main__':
        main()

