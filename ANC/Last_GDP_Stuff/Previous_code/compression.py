from scipy import weave
from numpy import exp

def calculate(x,N,env,T,CR,KW,MG,y):
    # """x = input signal (length N)
    # N = length of signal 
    # env = the envelope of the signal x (length N)
    # T = Threshhold (0 dB)
    # CR = Compression ratio (1)
    # KW = Knee width (10)
    # MG = Makeup gain (0)
    # y = An empty array for the output to be assigned to"""
	c_code = """

	int i = 0;
	double ydB;

	while (i < N) {

		double xdB = 20*log10(fabs(x[i]));
		double envdB = 20*log10(fabs(env[i]));
        // converts input x and envelope env to decibels


        // LOWER GAIN
		if ( ( 2 * ( envdB - T ) ) < - KW ) {
			ydB = xdB;
		}
        //below the lower knee, the input and output relationship is 1 to 1


        // MIDDLE GAIN
		else if ( ( 2 * abs ( envdB - T ) ) <= KW ) {
			ydB = xdB+(1/CR-1)*pow((xdB-T+(KW/2)),2)/(2*KW);
		}


        // UPPER GAIN
		else if ( ( 2 * ( envdB - T ) ) > KW ) {
			ydB = T+((xdB-T)/CR);
		}


		ydB = ydB + MG;
        // applies the makeup gain used to correct all the data

		y[i] = pow(10,ydB/20);
        // takes the anti-log of y

		if ( x[i] < 0 ) {
			y[i] = -y[i];
		}

		i++;
	}
	return_val = 0;
	"""
	return weave.inline(c_code,['x','N','env','T','CR','KW','MG','y'],compiler='gcc')
