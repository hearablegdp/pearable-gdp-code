# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import matplotlib.pyplot as plt



# function to read in complex valued data
def to_complex(field):
	return complex(field.replace(' ', '').replace('+-', '-').replace('i', 'j'))


# read in transfer upstream functions from txt file
#H1 = np.loadtxt("0 test.txt",converters={0: to_complex}, dtype=np.complex128)
#H2 = np.loadtxt("1 test.txt",converters={0: to_complex}, dtype=np.complex128)
#H3 = np.loadtxt("2 test.txt",converters={0: to_complex}, dtype=np.complex128)

# read in NLMS output without feedback control
def main():
	# read in NLMS output without feedback control
	NLMS = np.loadtxt('NLMS output.txt')
	
	print(NLMS.shape)
	print(NLMS.dtype)
	
	fs = 32000.0
	
	# FFT of the data
	FFT = np.fft.fft(NLMS)
	freqs = np.fft.fftfreq(FFT.size, 1/fs)

	# plot of the FFT
	plt.figure()
	plt.plot(freqs, FFT)
	plt.xlim(0, fs/2)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('NLMS(f)')
	plt.savefig('Feedback NLMS')
	plt.show()

	
	
	NLMS_upstream_feedback = np.loadtxt('NLMS output upstream feedback.txt')
	
	print(NLMS.shape)
	print(NLMS.dtype)
	
	fs = 32000.0
	
	# FFT of the data
	FFT = np.fft.fft(NLMS)
	freqs = np.fft.fftfreq(FFT.size, 1/fs)

	# plot of the FFT
	plt.figure()
	plt.plot(freqs, FFT)
	plt.xlim(0, fs/2)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('NLMS(f)')
	plt.savefig('Upstream Feedback NLMS')
	plt.show()




	NLMS_notch_feedback = np.loadtxt('NLMS output notch feedback.txt')
	
	print(NLMS.shape)
	print(NLMS.dtype)
	
	fs = 32000.0
	
	# FFT of the data
	FFT = np.fft.fft(NLMS)
	freqs = np.fft.fftfreq(FFT.size, 1/fs)

	# plot of the FFT

	plt.figure()
	plt.plot(freqs, FFT)
	plt.xlim(0, fs/2)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('NLMS(f)')
	plt.savefig('Upstream Feedback NLMS')
	plt.show()
	


if __name__ == '__main__':
        main()
