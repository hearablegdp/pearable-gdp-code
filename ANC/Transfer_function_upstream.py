# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import alsaaudio
import sys
import time
from scipy.io import wavfile
import numpy as np
from numpy import reshape
import matplotlib.pyplot as plt
import wave
import padasip as pad
import scipy.signal as signal
from copy import deepcopy
from Feedback_control import feedbk_cnt

# audio variables
channels = 2                        # number of channels
sample_size = 2                     # number of analog samples in bytes
frame_size = channels*sample_size   # one sample being played irrespective of channels or bits
									# for stereo and S16_LE: 2*(16 bits) = 4 bytes per sample
fs = 44100							# sample rate in Hz
bps_rate = sample_size*fs 			# bytes per second data required to sustain the system. =channels*sample_size*analog_rate
									# = frame_size*analog_rate
bps_rate = fs*frame_size   			# the number of bytes that are processed per unit of time.
period_size = 512                  # The number of frames in between each hardware interrupt.
									# Currently this will create an interrupt every 0.02s

###### set input device parameters
inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, 'hw:0,0')
inp.setchannels(2)
inp.setrate(fs)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
inp.setperiodsize(period_size)


###### set output device parameters
out = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, alsaaudio.PCM_NORMAL, 'hw:0,0')
out.setchannels(2)
out.setrate(fs)
out.setformat(alsaaudio.PCM_FORMAT_S16_LE)
out.setperiodsize(period_size)

##### Filter parameters #####
n = 100 			# filter length (number of taps)
mu = 0.0000000001  	# step size
itera = 600	 		# number of iterations

# create white noise
mean = 0
std = 1 
num_samples = 100000
noise = np.random.normal(mean, std, size=num_samples)

# read in pink noise: 44.1 kHz, 16 bit, mono.
fsp, pink = wavfile.read('Pink noise.wav')


# create pink noise matrix
segments = len(pink)/period_size
pink_noise = np.zeros((itera, period_size))
start = 0
while start < itera - 1:
	pink_noise[start, :] = pink[start*period_size:(start+1)*period_size]
	start += 1

### To LMS
def main():

	pink_est_out = np.zeros((itera, period_size))			# empty matrix for estimate of pink noise data
	ref_out = np.zeros((itera, period_size))				# empty matrix for error data
	coeffs = np.zeros((itera, n))							# empty matrix for coefficients
	
	###### To LMS #####
	coeffs[0, :] = signal.firwin(n, cutoff=0.3, window='hamming')		# original coefficients
	count = 0															# itertive counter
	
	## send white noise to secondary source - might put this on a separate script
#	pink_out = np.array(pink, dtype=np.int16)		# convert back to alsa data
	#out.write(pink_out)

	while count < itera - 1:	# might want to change this - look at last coefficients to tell.

		l, data = inp.read()			# read mic data and length of buffer
		if l == period_size:    		# check that the data collection is equal to the period_size

			num = np.fromstring(data, dtype='int16')    # convert to numerical data

			# split left and right input channels and obtain only ref	
			r = deepcopy(num)
			right = r[1::2]
			ref_out[count, :] = right

			# create time history of ref signal the length of the filter - could window signal instead
			x = pad.input_from_history(pink_noise[count, :], (n-1), bias=True)
			transpose = np.transpose(x)

			# filter all samples in the buffer with the filter coefficients
			transpose = np.transpose(x)							# take the transpose of the time history of ref
			pink_est = np.dot(coeffs[count, :], transpose)		# filter the time history of ref

			# pad estimate to the same length as the error signal
			padlen = len(right) - len(x) + 1						# pad length
			padd = np.array(pink_est[-1*padlen:-1])				# capture the end of the ref estimate
			pink_est_pad = np.concatenate((pink_est, padd))   	# pad the end of the ref est so it is the right length


			# obtain error signal
			error = right - pink_est_pad

			# update the coefficients
			pink_est_out[count, :] = pink_est_pad									# store estimate of pink noise
			coeffs[count+1, :] = coeffs[count, :] + mu*error[-1*(n+1):-1]*x[-1, :]	# update coefficients

			count += 1
	
	# re-arrange signals into a vector
	ref_sig = ref_out.ravel()
	est_sig = pink_est_out.ravel()
	pink_sig = pink[0:len(ref_sig)]
			
	# compute transfer function from PSD and CPSD
	#f1, Pxy = signal.csd(pink_sig, ref_sig, fs, nperseg=256)
	#f2, Pxx= signal.welch(pink_sig, fs, nperseg=256)
	#G = Pxy/Pxx
	
	## Plot of G
	#plt.figure()
	#plt.plot(f1, 20*np.log10(np.abs(G)))
	#plt.xlabel('Frequency (Hz)')
	#plt.ylabel('Magnitude (dB)')
	#plt.title('Transfer function of G')
	#plt.show()
	
	#plt.figure()
	#G_phase = np.unwrap(np.arctan2(np.imag(G), np.real(G)))
	#plt.plot(f1, G_phase)
	#plt.xlabel('Frequency (Hz)')
	#plt.ylabel('Phase (degrees)')
	#plt.title('Transfer function of G phase')
	#plt.show()

	## Plot of the IR from LMS coefficients
	#plt.figure()
	#plt.plot(signal.firwin(n, cutoff=0.3, window="hamming"), label='Original')
	#plt.plot(coeffs[1, :], label='First')
	#plt.plot(coeffs[2, :], label='Second')
	#plt.plot(coeffs[-3, :], label='Third last')
	#plt.plot(coeffs[-2, :], label='Second last')
	#plt.plot(coeffs[-1, :], label='Last')
	#plt.title('Impulse response of plant')
	#plt.xlabel('Coefficient number')
	#plt.ylabel('Magnitude')
	#plt.legend()
	#plt.show()
	
	# plot of the error and input signal
	plt.subplot(211)
	plt.plot(pink[5000:6000], label='reference')
	plt.plot(est_sig[5000:6000], label='adaptive filter')
	plt.plot(ref_sig[5000:6000], label='error')
	plt.ylabel('Amplitude')
	plt.title('Reference signal, error signal and adaptive filter output')
	plt.legend()
	plt.subplot(212)
	plt.legend()
	plt.ylabel('Amplitude')
	plt.xlabel('Samples')
	plt.show()
	
	# Get frequency response of final LMS filter
	b = coeffs[-1, :]
	a=1
	w, h = signal.freqz(coeffs[-1, :], a=1)

	# Plot of the transfer function of LMS
	plt.figure()
	plt.plot((w*fs)/(2*np.pi), 20*np.log10(np.abs(h)))
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Magnitude (dB)')
	plt.title('Transfer function')
	plt.show()

	## PLot of the phase of LMS
	#plt.figure()
	#h_Phase = np.unwrap(np.arctan2(np.imag(h), np.real(h)))
	#plt.plot(w*fs/(2*np.pi), h_Phase)
	#plt.xlabel('Frequency (Hz)')
	#plt.ylabel('Phase (degrees)')
	#plt.title('Transfer function phase')
	#plt.show()
	
	#[-8.41610671e-01 -6.42065627e-01 -4.61629786e-01 -3.04440326e-01
 #-1.20946439e-01  4.89895156e-01  4.78330913e-01  4.27308110e-01
  #2.46420724e-01  1.81741643e-01  4.17586028e-01  1.30717867e-01
 #-4.17889997e-01 -5.53158480e-01 -1.00248676e+00 -4.36575176e-01
  #2.84652516e-02 -3.37466653e-01 -9.55564222e-02  2.16788782e-02
 #-1.66954522e-01  5.01014931e-01 -4.99497597e-02  3.78688785e-01
  #3.56858403e-01 -3.39695987e-01 -2.07821613e-01  8.06150272e-02
 #-7.52677523e-01 -3.11005713e-01  2.05185276e-01  1.46961531e-01
  #1.86255127e-01  8.43320962e-02  2.64175727e-01  3.86051276e-01
  #1.21795365e-01  2.93542914e-01  2.75380654e-01  5.42481355e-01
  #3.41576740e-01 -4.61243861e-01  5.88636117e-02  8.25316929e-01
  #3.56603808e-01  2.53274138e-01  2.14169493e-01  7.15878483e-02
  #2.43705445e-01  4.01633041e-01  4.46797018e-01  3.88096635e-01
 #-2.60413127e-02  8.38827026e-02 -6.52141647e-02  4.54539981e-01
  #2.55000359e-01  1.41443770e-01 -3.75455789e-02  8.30778705e-02
 #-2.04566823e-01 -2.46651239e-01 -2.55095170e-01 -2.32167952e-02
  #7.83925618e-01  5.41292388e-01  2.18871158e-01  4.08564508e-01
  #3.70423962e-01  1.37334093e-01  6.53026177e-01  6.52448185e-01
  #3.85788253e-01  1.46124560e-01  2.02747391e-01 -5.43087916e-02
 #-3.46831139e-01 -7.89365780e-01 -7.42547572e-01 -2.82567145e-01
 #-2.19973263e-01 -2.21297883e-01  4.02590855e-01  5.35646592e-01
  #1.76382265e-02  2.46506989e-01  1.14055111e+00  2.46384294e-01
  #1.28583724e-02 -1.50010195e-01 -1.82780072e-01 -4.56020251e-02
 #-4.10071402e-01 -1.95264991e-01 -6.69291778e-01 -3.61650779e-01
 #-4.52665071e-01 -7.67893178e-01 -4.14927876e-01  2.38817275e-04]
 
 #[-5.18789895e-01 -4.30769918e-01 -3.14391638e-01 -3.00490894e-01
 #-1.66965268e-01  3.09831825e-01  3.63742180e-01  2.78533016e-01
  #2.10957862e-01  1.42284437e-01  4.24864150e-01  6.74022346e-02
 #-3.59736930e-01 -4.45220782e-01 -7.49357819e-01 -3.76130754e-01
  #2.04933524e-02 -2.35879945e-01 -3.76141612e-02 -7.52270317e-02
 #-2.12680557e-01  1.89626890e-01 -1.51871942e-01  1.77639429e-01
  #1.21736528e-01 -3.11039145e-01 -1.81052755e-01  7.83396660e-02
 #-5.41287047e-01 -2.35258693e-01  1.92906861e-01  1.61907588e-01
  #1.67494279e-01  1.46853183e-01  2.32915275e-01  2.89362629e-01
  #1.03001845e-01  2.54607445e-01  2.00891290e-01  4.18918235e-01
  #2.46868904e-01 -2.24363545e-01  1.53013246e-01  6.79918080e-01
  #2.65221723e-01  8.29232879e-02  1.23662126e-01  1.05999115e-01
  #2.22919269e-01  3.32499133e-01  3.22563129e-01  2.99151823e-01
  #2.48253822e-02 -2.77191440e-02 -1.58090271e-01  2.67565974e-01
  #2.25650789e-01  7.21588779e-02 -3.50003976e-02 -1.42112733e-02
 #-1.69822345e-01 -1.55845866e-01 -1.10187118e-01  2.82494337e-03
  #5.47653448e-01  2.89675531e-01  1.37066598e-01  3.21373927e-01
  #2.24730522e-01  1.21015773e-01  3.90644459e-01  3.91715316e-01
  #2.57623801e-01  6.36185481e-02  1.52640628e-01 -1.32497521e-01
 #-1.95065788e-01 -6.35460319e-01 -5.44267157e-01 -1.56491480e-01
 #-1.08625928e-01 -1.23922075e-01  2.64697108e-01  4.10851313e-01
  #6.48908286e-02  2.39861377e-01  9.22026523e-01  3.28793910e-01
  #9.99770367e-02 -1.55579037e-01 -1.83794165e-01 -8.38269622e-02
 #-2.85680355e-01 -2.56184704e-01 -5.57219841e-01 -2.20094452e-01
 #-2.50208873e-01 -4.99317573e-01 -3.07148543e-01  2.37956871e-04]


if __name__ == '__main__':
        main()
		

