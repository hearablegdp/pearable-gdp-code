# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import alsaaudio
import sys
import time
from scipy.io import wavfile
import numpy as np
from numpy import reshape
import matplotlib.pyplot as plt
import wave
import padasip as pad
import scipy.signal as signal
from copy import deepcopy
from Feedback_control import feedbk_cnt
import threading
import time


# audio variables
channels = 2                        # number of channels
fs = 44100							# sample rate in Hz
period_size = 1024                   # The number of frames in between each hardware interrupt.

# set input device parameters
inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, 'hw:0,0')
inp.setchannels(2)
inp.setrate(fs)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
inp.setperiodsize(period_size)


# set output device parameters
out = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, alsaaudio.PCM_NORMAL, 'hw:0,0')
out.setchannels(2)
out.setrate(fs)
out.setformat(alsaaudio.PCM_FORMAT_S16_LE)
out.setperiodsize(period_size)


# Filter parameters
n = 4000 			# filter length (number of taps)
mu = 1e-11  		# step size
itera = 1000	 		# number of iterations

# create white noise
mean = 0
std = 1 
num_samples = 100000
noise = np.random.normal(mean, std, size=num_samples)

# read in pink noise: 44.1 kHz, 16 bit, mono.
fsp, pink = wavfile.read('Pink noise.wav')

# create pink noise matrix for LMS
pink_noise = np.zeros((itera, period_size))
start = 0
while start < itera - 1:
	pink_noise[start, :] = pink[start*period_size:(start+1)*period_size]
	start += 1


# Define a new subclass of thread class for the LS
class LS(threading.Thread):

	# override innit method to add additional arguments
	def __init__(self, threadID, name):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name

   # override the run method to implement what the tread should do.
	def run(self):
		print "Starting " + self.name
		ls()
		print "Exiting " + self.name

def ls():
	data_proc = np.array(pink, dtype=np.int16)
	out.write(data_proc)
	return


def main():
	
	# Create new instance of threads which will implement run. 
	thread1 = LS(1, "Thread-1")
	thread1.start()			# Start LS threads

	pink_est_out = np.zeros((itera, period_size))			# empty matrix for estimate of pink noise data
	er_mic_out = np.zeros((itera, period_size))				# empty matrix for mic data
	coeffs = np.zeros((itera, n))							# empty matrix for coefficients
	error_out = np.zeros((itera, period_size))				# empty matrix for error signal

	##### To LMS #####
	coeffs[0, :] = signal.firwin(n, cutoff=0.3, window='hamming')		# original coefficients
	count = 5

	
	while count < itera - 1:

		l, data = inp.read()				# read mic data and length of buffer
		if l == period_size:    			# check that the data collection is equal to the period_size

			# convert mic data to numerical data
			num = np.fromstring(data, dtype='int16')

			# split left and right input channels and keep only error mic
			l = deepcopy(num)
			left = l[0::2]		
			er_mic_out[count, :] = left		# store error data

			# overlap pink_noise buffers to create time history
			buffer5 = pink_noise[count-4, :]
			buffer4 = pink_noise[count-3, :]
			buffer3 = pink_noise[count-2, :]
			buffer2 = pink_noise[count-1, :]
			overlap = n - (3*period_size) - 1
			buffer1 = pink_noise[count, :]
			
			joined = np.concatenate((buffer5[-1*overlap:], buffer4, buffer3, buffer2, buffer1))
			time_hist = pad.input_from_history(joined, n, bias=False)
			x = time_hist[:, ::-1]

			# filter all samples in the buffer with the FIR filter
			transpose = np.transpose(x)							# take the transpose of the time history of input
			pink_est = np.dot(coeffs[count, :], transpose)		# filter the time history of the input

			# obtain error signal  # check this shouldn't be squared
			error = left - pink_est
			error_out[count, :] = error

			# update the coefficients with the last samples of the error sigbal ----- ???? - could also take the average
			pink_est_out[count, :] = pink_est									# store estimate of pink noise
			coeffs[count+1, :] = coeffs[count, :] + mu*error[-1]*x[-1, :]			# update coefficients

			count += 1


	
	# re-arrange signals into a vector
	error_mic = er_mic_out.ravel()
	LMS_out = pink_est_out.ravel()
	error_sig = error_out.ravel()

	print(len(LMS_out))
	print(len(error_mic))
	
	# Plot of the IR from LMS coefficients
	plt.figure()
	plt.plot(signal.firwin(n, cutoff=0.3, window="hamming"), label='Original')
	plt.plot(coeffs[1, :], label='First')
	plt.plot(coeffs[2, :], label='Second')
	plt.plot(coeffs[-3, :], label='Third last')
	plt.plot(coeffs[-2, :], label='Second last')
	plt.plot(coeffs[-1, :], label='Last')
	plt.title('Impulse response of plant')
	plt.xlabel('Sample')
	plt.ylabel('Magnitude')
	plt.legend()
	plt.show()
	
	## plot of the error and input signal
	#plt.figure()
##	plt.plot(pink[20000:24800], label='pink noise')
	#plt.plot(LMS_out, label='LMS output')
	#plt.plot(error_mic, label='mic signal')
	#plt.plot(error_sig, label='error sig')
	#plt.ylabel('Amplitude')
	#plt.title('Reference signal, error signal and adaptive filter output')
	#plt.legend()
	#plt.show()
	
	# Get frequency response of final LMS filter
	b = coeffs[-1, :]
	a=1
	w, h = signal.freqz(coeffs[-1, :], a=1)

	# Plot of the transfer function of LMS
	plt.figure()
	plt.semilogx((w*fs)/(2*np.pi), 20*np.log10(np.abs(h)), label='h')
 	plt.xlim(0, 1000)
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Magnitude (dB)')
	plt.title('Transfer function LMS')
	plt.show()

	## PLot of the phase of LMS
	#plt.figure()
	#h_Phase = np.rad2deg(np.unwrap(np.angle(h, deg=0)))
	#plt.semilogx(w*fs/(2*np.pi), h_Phase, label='h')
	#plt.xlim(0, 1000)
	#plt.xlabel('Frequency (Hz)')
	#plt.ylabel('Phase (degrees)')
	#plt.title('Transfer function phase LMS')
	#plt.show()
	
	## Plot of both FRF magnitudes
	#plt.figure()
	#plt.plot((w*fs)/(2*np.pi), 20*np.log10(np.abs(h)), label='h')
	#plt.plot(f1, 20*np.log10(np.abs(G)), label='G')
	#plt.xlim(0, 1000)
	#plt.ylabel('Magnitude (dB)')
	#plt.title('FRF Magnitude')
	#plt.legend()
	#plt.show()
	
	## PLot of the both FRF phase
	#plt.figure()
	#plt.plot(w*fs/(2*np.pi), h_Phase, label='h')
	#plt.plot(f1, G_phase, label='G')
	#plt.xlabel('Frequency (Hz)')
	#plt.ylabel('Phase (degrees)')
	#plt.title('FRF phase')
	#plt.show()

if __name__ == '__main__':
        main()
		

