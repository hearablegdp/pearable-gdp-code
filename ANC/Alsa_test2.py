# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import alsaaudio as audio
from scipy.io import wavfile
# import visr
# visr::audiointerfaces::AudioInterface

periodsize = 1024
audioformat = audio.PCM_FORMAT_S16_LE
channels = 2
framerate = 44100
card_info = {}
for device_number, card_name in enumerate(audio.cards()):
    card_info[card_name] = "hw:%s,0" % device_number
print(card_info)


inp = audio.PCM(audio.PCM_CAPTURE, audio.PCM_NONBLOCK)
inp.setchannels(channels)
inp.setrate(framerate)
inp.setformat(audioformat)
inp.setperiodsize(periodsize)

out = audio.PCM(audio.PCM_PLAYBACK)
out.setchannels(channels)         # set number of output channels
out.setrate(framerate)            # set framerate
out.setformat(audioformat)        # set output format
out.setperiodsize(periodsize)     # set periodsize

#buffer = new_fvec(framesize, channels)

data = inp.read()
print(data)
out



