#!/bin/bash +x
set -e
# Credits to LukasJapan https://github.com/lukasjapan/bt-speaker
# This script has been modified by the pearable gdp group 2017/18
# Tested with the "2018-04-17-raspbian-stretch-desktop.img" image. Working however requires initial command line boot and slight edits as described

# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Welcome
echo
echo "Welcome to the Pearable Bluetooth Audio Reciever Install"
# Dependencies
echo
echo "Installing dependencies..."
apt-get update
apt-get --yes --force-yes install git bluez python python-gobject python-cffi python-dbus python-alsaaudio python-configparser sound-theme-freedesktop vorbis-tools
echo "done."

# Add bluetoothaudio user if not already existing
echo
echo "Adding bluetoothaudio user..."
id -u bluetoothaudio &>/dev/null || useradd bluetoothaudio -G audio
# Also add user to bluetooth group if it exists (required in debian stretch)
getent group bluetooth &>/dev/null && usermod -a -G bluetooth bluetoothaudio
echo "done."
echo
echo "Adding pi user to bluetooth group"
adduser pi bluetooth
echo "done."

# Copy bluetooth audio files from our git or clone if not already present
# Originally was downloaded from bt-speaker from lukas git. Clone our git if not there already
echo
cd /home/pi
if [ -d pearable-gdp-code ]; then
  echo "Updating Bluetooth Audio..."
  cd pearable-gdp-code/Configuration\ Files/App\ and\ BT\ audio/BluetoothAudio && git pull && git checkout ${1:master}
else
  echo "Downloading Bluetooth Audio from Pearable Repository..."
  git clone https://bitbucket.org/hearablegdp/pearable-gdp-code.git
  cd pearable-gdp-code/Configuration\ Files/App\ and\ BT\ audio/BluetoothAudio && git pull && git checkout ${1:master}
fi
echo "Bluetooth Audio updated."

# Move BluetoothAudio from Bitbucket repo to home folder
echo
echo "Copying Bluetooth audio folder from Bitbucket repo to local home directory"
cp -r /home/pi/pearable-gdp-code/Configuration\ Files/App\ and\ BT\ audio/BluetoothAudio /home/pi/BluetoothAudio
echo "done."

# Copy services folder from bitbucket to local home directory
echo
echo "Copying services folder from Bitbucket repo to local home directory"
cp -r /home/pi/pearable-gdp-code/Service\ Files/ /home/pi/
echo "done."


# Prepare config
echo "Are you using the audio injector soundcard? If no will be configured for Standard Pi 3. (Enter 1/2)"
select yn in "Yes" "No"; do
case $yn in
Yes )
echo "Setting up for AudioInjector"
cp -n /home/pi/BluetoothAudio/config.ini.audioinjector.default /home/pi/BluetoothAudio/config.ini;
echo "done.";
break;;

No )
echo "Setting up for Raspberry Pi without soundcard"
cp -n /home/pi/BluetoothAudio/config.ini.pi.default /home/pi/BluetoothAudio/config.ini;
echo "done.";
break;;

esac
done


echo "Adding commands to bashrc so that pi boots in discoverable mode, and allows for pairing"
echo "### Added by Bluetooth Audio Install ###">> /home/pi/.bashrc
echo "sudo hciconfig hci0 piscan">> /home/pi/.bashrc
echo "done."
echo
echo "Disabling WiFi/Wireless card in bashrc"
echo "sudo ifconfig wlan0 down">> /home/pi/.bashrc
echo "done."
# cp -n ~/pearable-gdp-code/Configuration\ Files/App\ and\ BT\ audio/BluetoothAudio/config.ini.default ~/BluetoothAudio/config.ini
# cp -n /opt/bt-speaker/hooks.default/connect /etc/bt_speaker/hooks/connect
# cp -n /opt/bt-speaker/hooks.default/disconnect /etc/bt_speaker/hooks/disconnect


# Install and start bluetooth audio daemon
echo
echo "Registering and starting bluetoothaudio service with systemd..."
systemctl enable /home/pi/Service\ Files/bluetoothaudio.service
systemctl daemon-reload
if [ "`systemctl is-active bluetoothaudio`" != "active" ]; then
  systemctl start bluetoothaudio
else
  systemctl restart bluetoothaudio
fi
  systemctl status bluetoothaudio --full --no-pager
echo "done."


# Finished
echo
echo "Bluetooth Audio has successfully been installed"
echo "Please now use run 'sudo raspi-config' and configure your Pi so that it boots into a command line, auto-logon to achieve full functionality."
echo "You can also use this tool to change the Hostname of the Pi"
