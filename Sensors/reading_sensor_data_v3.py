# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Simple example of reading the MCP3008 analog input channels and printing
# them all out.
# Author: Tony DiCola
# License: Public Domain

# Modified by: Christabel Goode, Jordy Williams, Karen Mortby
# Date: 12/12/17

import time
import numpy as np

# Import SPI library (for hardware SPI) and MCP3008 library.
# import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008


# Software SPI configuration:
CLK  = 12  # 12
MISO = 23
MOSI = 24
CS   = 25
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)


def temp(sensor, channel):
  places = 2  # round data to 2 decimal places
  data = mcp.read_adc(channel)
#  print(data)
  n_bits = 10
  volts = (data*3.3)/float(2**n_bits)  #maybe 1023 not 1024
  R = 10000  # resistance of resistor in series with thermistor (same as resistance of thermistor)
  ohms = ((1/volts)*(3.3*R)) - R
  lnohm = np.log(ohms)
  # https://www.thermistor.com/calculators
#  volts = round(volts, places)
  
  if sensor == 'tmp36':
    min_temp = -50
    max_temp = 280

  elif sensor == 'elegoo_temp':
    min_temp = -55
    max_temp = 150
##
##  #10kOhm
##  a = 0.001837878544850
##  b = 0.000156630677797
##  c = 0.000000092546684

##      5kOhm
##      a = 0.001944288892353
##      b = 0.000158046593663
##      c = 0.000000100394548
##
##      1kOhm
##      a = 0.002197222470870
##      b = 0.000161097632222
##      c = 0.000000125008328

  elif sensor == 'wire_leaded':  # super tiny thermistor
    min_temp = -55
    max_temp = 300

##  a = -1.151182577e-3  # from thinksrs.com
##  b = 5.433275852e-4
##  c = -6.387077840e-7
##
  elif sensor == 'radial_left':  # mustard coloured thermistor
    min_temp = -40
    max_temp = 125
##    a = 0.001832260264881  # from thermistor.com (steinhart equation)
##    b = 0.000157821215964
##    c = 0.000000085400020

  elif sensor =='radial_right':
    min_temp = -40
    max_temp = 125


  else:
    print('Invalid sensor name')

##  min_temp = -40
##  max_temp = 125
##  a = 0.001832260264881  # from thermistor.com (steinhart equation)
##  b = 0.000157821215964
##  c = 0.000000085400020

  #Steinhart Equation
  # T = 1/(a + b[ln(ohm)] + c[ln(ohm)]^3)

##  t1 = (b*lnohm) # b[ln(ohm)]
##  c2 = c*lnohm # c[ln(ohm)]
##  t2 = c2**3 # c[ln(ohm)]^3

##  temp = 1/(a + t1 + t2) #calcualte temperature
##
##  tempc = temp - 273.15 - 4 #K to C
##  # the -4 is error correction for bad python math
##  print(tempc)
##  return tempc

  temp_range = max_temp - min_temp
  #temp = ((data*temp_range)/float(2**n_bits)) + min_temp  # this is giving ~56 degrees atm
  temp = volts/(3.3/temp_range) + min_temp
  temp = round(temp, places)

  return temp
  

def print_data(data0=0, data1=0, data2=0, data3=0, data4=0, data5=0, data6=0, data7=0, delay=5):
  print('Reading sensor data, press Ctrl-C to quit...')
  print('| {} | {} | {} | {} | {} | {} | {} | {} |'.format('TMP36','Radial_L', 'Radial_R', 'Elegoo', 'Wire', ' ', ' ', ' '))
  # Print nice channel column headers.
  while True:
    
    print('| {} | {} | {} | {} | {} | {} | {} | {} |'.format(data0, data1, data2, data3, data4, data5, data6, data7))

    time.sleep(delay)


# Define sensor channels
channel_tmp36 = 0
channel_radial_left = 1
channel_radial_right = 2
channel_elegoo = 3
channel_wire_leaded = 4

# Call functions to get temperatures
# in  app code the temp function is called on refresh button setup
temp_tmp36 = temp('tmp36', channel_tmp36)
temp_radial_left = temp('radial_left', channel_radial_left)
temp_radial_right = temp('radial_right', channel_radial_right)
temp_elegoo = temp('elegoo_temp', channel_elegoo)
temp_wire_leaded = temp('wire_leaded', channel_wire_leaded)

temperature = print_data(temp_tmp36, temp_radial_left, temp_radial_right, temp_elegoo, temp_wire_leaded)
