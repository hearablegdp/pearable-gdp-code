# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License. 

# Author: Jordan Williams

import os
import datetime
import numpy as np

def readLog(N=10):
    """This function was written to read the pearable sensor log
    The log runs every min and logs the current sensor values.
    The current date's log is read, the past N (input) mins are read. If
    the log does not exist or there is not enough data that has been requested
    errors are returned.
    arrays of the past N mins of data are returned as follows
    timeData, rightHeartRate, LeftHeartRate, tempLeft, tempRight, tmp36 """ 
    os.chdir('/home/pi/pearable')
    date = str(datetime.datetime.now()).split(" ")[0].replace("-", "_")
    timeData, heartR, heartL, tempLeft, tempRight, tmp36 = [], [], [], [], [], []
    if os.path.exists('/home/pi/pearable/sensordata_{}.log'.format(date)):
	    logFile = open('sensordata_{}.log'.format(date))
    else:
        raise IOError("The sensor log file doesn't exist yet. Please check the logsensor.service is running")
    data = logFile.readlines()[-N:]  # Reading last N lines from the log file
    if N > len(data):
        raise IOError("The log does not have {} mins of data. Please wait longer".format(N))
    for i in range(len(data)):
        date, time = data[i].split(':')[0].split('\t')
        timeData.append(date + ':' + time)
        tempValues = (data[i].split(':')[2].split("\t"))
        heartValues = (data[i].split(':')[3].split("\t"))
        heartL.append(heartValues[0])
        heartR.append(heartValues[1])
        tempLeft.append(tempValues[2])
        tempRight.append(tempValues[1])
        tmp36.append(tempValues[0])
    return timeData, heartR, heartL, tempLeft, tempRight, tmp36
