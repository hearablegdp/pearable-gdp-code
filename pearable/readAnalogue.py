# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Simple example of reading the MCP3008 analog input channels and printing
# them all out.
# Author: Tony DiCola - modifyed by Jordan Williams, Christabel Goode, Karen Mortby
# License: Public Domain
import time
import numpy as np

# Import SPI library (for hardware SPI) and MCP3008 library.
# import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008

# Import interpolate function for radial thermistor values
import scipy.interpolate as sc

# Software SPI configuration:
CLK  = 12 # This was changed from original value to not use same clock as soundcard
MISO = 23
MOSI = 24
CS   = 25
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)


def temp(sensor, channel):
  places = 2  # round data to 2 decimal places
  data = mcp.read_adc(channel)
#  print(data)
  n_bits = 10
  volts = (data*3.3)/float(2**n_bits)  #maybe 1023 not 1024
  R = 10000  # resistance of resistor in series with thermistor (same as resistance of thermistor)
  ohms = ((1/volts)*(3.3*R)) - R
  lnohm = np.log(ohms)
  # https://www.thermistor.com/calculators
#  volts = round(volts, places)
  
  if sensor == 'tmp36':
    min_temp = -50
    max_temp = 280
    temp_range = max_temp - min_temp
    temp = volts/(3.3/temp_range) + min_temp

  elif sensor == ('radial'):  # mustard coloured thermistor
    min_temp = -40
    max_temp = 125

    # Interpolation from resistance values on data sheet
    temps = np.arange(-40, 130, 5)
    RT = np.array([332.1, 239.9, 175.2, 129.3, 96.36, 72.5, 55.05, 42.16, 32.56, 25.34, 19.87, 15.7, 12.49, 10.0, 8.059, 6.535, 5.330, 4.372, 3.606, 2.989, 2.49, 2.084, 1.753, 1.481, 1.256, 1.070, 0.9154, 0.7860, 0.6773, 0.5858, 0.5083, 0.4426, 0.3866, 0.3387])*1000
    # Using interp1d to generate a function based off the data sheet - fill_value was needed to accomadte values lying outside the range
    # Got 'outside bounds of x_new error'
    temp_function = sc.interp1d(RT, temps, kind='linear', fill_value='extrapolate')
    temp = temp_function(ohms)

##
##    #Steinhart Equation
##    "T = 1/(a + b[ln(ohm)] + c[ln(ohm)]^3)"  # just for ref.
##
##    a = 0.001832260264881  # from thermistor.com (steinhart equation)
##    b = 0.000157821215964
##    c = 0.000000085400020
##
##    t1 = (b*lnohm) # b[ln(ohm)]
##    c2 = c*lnohm # c[ln(ohm)]
##    t2 = c2**3 # c[ln(ohm)]^3
##
##    temper = 1/(a + t1 + t2) # calculate temperature
##
##    temp = temper - 273.15 - 4 #K to C
##    # the -4 is error correction
##
##    # a = -1.151182577e-3  # from thinksrs.com
##    # b = 5.433275852e-4
##    # c = -6.387077840e-7


  elif sensor == 'elegoo_temp':
    min_temp = -55
    max_temp = 150


    #10kOhm
    a = 0.001837878544850
    b = 0.000156630677797
    c = 0.000000092546684

    #Steinhart Equation
    "T = 1/(a + b[ln(ohm)] + c[ln(ohm)]^3)"  # don't uncomment. just for ref.

    t1 = (b*lnohm) # b[ln(ohm)]
    c2 = c*lnohm # c[ln(ohm)]
    t2 = c2**3 # c[ln(ohm)]^3

    temper = 1/(a + t1 + t2) # calculate temperature

    temp = temper - 273.15 - 4 #K to C
    # the -4 is error correction

  elif sensor == 'wire_leaded':  # super tiny thermistor
    min_temp = -55
    max_temp = 300

    temp_range = max_temp - min_temp
    temp = volts/(3.3/temp_range) + min_temp


  else:
    print('Invalid sensor name')


  #temp_range = max_temp - min_temp
  #temp = ((data*temp_range)/float(2**n_bits)) + min_temp  # this is giving ~56 degrees atm
  #temp = volts/(3.3/temp_range) + min_temp
  #temp = round(temp, places)

  return temp
  

def print_data(data0=0, data1=0, data2=0, data3=0, data4=0, data5=0, data6=0, data7=0, delay=5):
  print('Reading sensor data, press Ctrl-C to quit...')
  print('| {} | {} | {} | {} | {} | {} | {} | {} |'.format('TMP36', 'Radial_left', 'Radial_right', 'Elegoo', 'Wire', ' ', ' ', ' '))
  # Print nice channel column headers.
  while True:
    
    print('| {} | {} | {} | {} | {} | {} | {} | {} |'.format(data0, data1, data2, data3, data4, data5, data6, data7))

    time.sleep(delay)


# Read heart rate
def readHeartRate(channel, n=3):
    # Channel for left and right channel int
    # n - int number of heartrates to find must be >1
    adc = mcp
    # initialization
    counter = 0
    curState = 0
    thresh = 700  # was 525
    P = 687 # was 512
    T = 687 # was 512
    stateChanged = 0
    sampleCounter = 0
    lastBeatTime = 0
    firstBeat = True
    secondBeat = False
    Pulse = False
    IBI = 600
    rate = [0]*10
    #amp = 100

    lastTime = int(time.time()*1000)
    heartBeats = np.zeros(n)
    # Main loop. use Ctrl-c to stop the code
    while counter < n:
        # read from the ADC
        Signal = adc.read_adc(channel)
        curTime = int(time.time()*1000)

        sampleCounter += curTime - lastTime;      #                   # keep track of the time in mS with this variable
        lastTime = curTime
        N = sampleCounter - lastBeatTime;     #  # monitor the time since the last beat to avoid noise
        #print N, Signal, curTime, sampleCounter, lastBeatTime

        ##  find the peak and trough of the pulse wave
        if Signal < thresh and N > (IBI/5.0)*3.0 :  #       # avoid dichrotic noise by waiting 3/5 of last IBI
            if Signal < T :                        # T is the trough
              T = Signal;                         # keep track of lowest point in pulse wave 

        if Signal > thresh and  Signal > P:           # thresh condition helps avoid noise
            P = Signal;                             # P is the peak
                                                # keep track of highest point in pulse wave

          #  NOW IT'S TIME TO LOOK FOR THE HEART BEAT
          # signal surges up in value every time there is a pulse
        if N > 250 :                                   # avoid high frequency noise
            if  (Signal > thresh) and  (Pulse == False) and  (N > (IBI/5.0)*3.0)  :       
              Pulse = True;                               # set the Pulse flag when we think there is a pulse
              IBI = sampleCounter - lastBeatTime;         # measure time between beats in mS
              lastBeatTime = sampleCounter;               # keep track of time for next pulse

              if secondBeat :                        # if this is the second beat, if secondBeat == TRUE
                secondBeat = False;                  # clear secondBeat flag
                for i in range(0,10):             # seed the running total to get a realisitic BPM at startup
                  rate[i] = IBI;                      

              if firstBeat :                        # if it's the first time we found a beat, if firstBeat == TRUE
                firstBeat = False;                   # clear firstBeat flag
                secondBeat = True;                   # set the second beat flag
                continue                              # IBI value is unreliable so discard it


              # keep a running total of the last 10 IBI values
              runningTotal = 0;                  # clear the runningTotal variable    

              for i in range(0,9):                # shift data in the rate array
                rate[i] = rate[i+1];                  # and drop the oldest IBI value 
                runningTotal += rate[i];              # add up the 9 oldest IBI values

              rate[9] = IBI;                          # add the latest IBI to the rate array
              runningTotal += rate[9];                # add the latest IBI to runningTotal
              runningTotal /= 10;                     # average the last 10 IBI values 
              BPM = 60000/runningTotal                # how many beats can fit into a minute? that's BPM!
              heartBeats[counter] = BPM
              print(BPM)
              counter += 1
              

        if Signal < thresh and Pulse == True :   # when the values are going down, the beat is over
            Pulse = False;                         # reset the Pulse flag so we can do it again
            amp = P - T;                           # get amplitude of the pulse wave
            thresh = amp/2 + T;                    # set thresh at 50% of the amplitude
            P = thresh;                            # reset these for next time
            T = thresh;

        if N > 2500 :                          # if 2.5 seconds go by without a beat
            thresh = 700;                          # set thresh default
            P = 600;                               # set P default
            T = 600;                               # set T default
            lastBeatTime = sampleCounter;          # bring the lastBeatTime up to date        
            firstBeat = True;                      # set these to avoid noise
            secondBeat = False;                    # when we get the heartbeat back
            heartBeats[counter] = None
            counter += 1
            print('not connected')
        time.sleep(0.005)
    # Test if None for any items eg no heart beats found. if so return 'none'
    # if not average all numbers that are present
    if np.all(np.isnan(heartBeats)):
       beat = 'None'
    else:  # some heart beats - remove nans
       beat = np.average(heartBeats[~np.isnan(heartBeats)])
    return beat


# Define sensor channels - used in raspibtsrv
channel_tmp36 = 0
channel_tempL = 1
channel_tempR = 2
channel_heartL = 3
channel_heartR = 4

# Call functions to get temperatures/HR
# in  app code the temp function is called on refresh button setup
