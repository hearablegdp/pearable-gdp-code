# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Authors: Christabel Goode and Karen Mortby
# Date: 23/11/17
#
# Reads the temperature from a digital temperature sensor (DS18B20)
# using a raspberry pi
# Saves output into a log file
#
# Found instructions for help using DS18B20 from:
# http://www.reuk.co.uk/wordpress/raspberry-pi/ds18b20-temperature-sensor-with-raspberry-pi/

import time
import datetime
import numpy as np

current_dt = datetime.datetime.now()  # gets current date and time from computer
dt = str(current_dt).split(" ")  # splits date and time
date = dt[0].replace('-', '_')  # replaces '-' with '_' in date
timestamp = (dt[1].replace(':', '_'))[:5]  # replaces ':' with '_' in time
# print (date, time)

#datafile = open("temperaturedata.log", 'w')

#datafile = open("temperaturedata_{}_{}.log".format(date, timestamp), 'w')

sensor_id = "28-000009226261"  # id number of the temp sensor
def getTemp(n=5, waitTime=1):
    """ 
    This function gets the average number of temp values
    waitTime - seconds by def is 1
    """
    counter = 0
    temps = np.zeros(n)
    while counter < n:
        tempfile = open("/sys/bus/w1/devices/{}/w1_slave".format(sensor_id))
        thetext = tempfile.read()
        tempfile.close()
        tempdata = thetext.split("\n")[1].split(" ")[9]  # split data between each line and remove spaces
        temperature = float(tempdata[2:])  #  dumpsturns temp from string to float
        temperature = temperature/1000  # gives temp in degrees C
        # datafile.write(str(temperature)+"\n")
        time.sleep(waitTime)  # takes temp every 1 second
        temps[counter] = temperature
        counter += 1  # stops saving temp after 5 measurments

# datafile.close()
    avg_temp = np.sum(temps)/n  # calculates average temperature from n measurements
    return avg_temp
# print ("Temperature for {} {} is ".format(date, timestamp), np.round(avg_temp, 1), "degrees C")

# have a button on the app to say "start measuring" and then measure n samples
